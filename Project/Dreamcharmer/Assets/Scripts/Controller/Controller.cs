﻿using System.Collections.Generic;
using UnityEngine;

/*
 *      Name:           Controller.cs
 *      Author:         Vincent Mills
 *      Version:        0.1.1 (Documented code.)
 *      Last Modified:  27 June 2017
 */

/// <summary> The Direction of the control axis that is needed. </summary>
public enum Direction
{
	HORIZONTAL,
	VERTICAL,
	SCROLL
}

public class Controller : MonoBehaviour
{

	public void Update()
	{
		KeyboardControls.Update();
		JoyControls.Update();
	}
}

/// <summary>
/// Controls class.
/// 
/// <para>
///		Keeps track of both keyboard and joystick controls. 
///		Use this for when you want to use input.
///	</para>
///	
/// </summary>
/// <remarks>
///		Static class. Cannot instantiate.
/// </remarks>
public static class Controls
{
	/// <summary> Gets the axis of the intended direction.</summary>
	/// <param name="d">The desired direction</param>
	/// <returns>The amount of input in the given direction (0-1) </returns>
	public static float GetAxis(Direction d)
	{
		switch (d)
		{
			case Direction.HORIZONTAL:
				var h = Input.GetAxis("Horizontal");
				if (Mathf.Abs(h) <= 0.25f)
					return KeyboardControls.GetHorizontal();
				return h;
			case Direction.VERTICAL:
				var v = Input.GetAxis("Vertical");
				if (Mathf.Abs(v) <= 0.25f)
					return KeyboardControls.GetVertical();
				return v;
			default:
				return Input.GetAxis("Mouse ScrollWheel");
		}
	}

	#region GetKey
	/// <summary> Checks whether or not a button is held down. </summary>
	/// <param name="action">The action key to reference.</param>
	/// <returns><c>true</c> if the button is being held. <c>false</c> otherwise. </returns>
	public static bool GetKey(string action)
	{
		return KeyboardControls.GetKey(action) || JoyControls.GetKey(action);
	}

	/// <summary> Checks whether or not a button was pressed. </summary>
	/// <param name="action">The action key to reference.</param>
	/// <returns><c>true</c> if the button was pressed. <c>false</c> otherwise. </returns>
	public static bool GetKeyDown(string action)
	{
		return KeyboardControls.GetKeyDown(action) || JoyControls.GetKeyDown(action);
	}

	/// <summary> Checks whether or not a button was let go. </summary>
	/// <param name="action">The action key to reference.</param>
	/// <returns><c>true</c> if the button was let go. <c>false</c> otherwise. </returns>
	public static bool GetKeyUp(string action)
	{
		return KeyboardControls.GetKeyUp(action) || JoyControls.GetKeyUp(action);
	}

	public static bool GetKeyPressed(string action)
	{
		return KeyboardControls.GetKeyPressed(action) || JoyControls.GetKeyPressed(action);
	}
	#endregion GetKey


	/// <summary> Checks if two actions are being pressed simultaneously. </summary>
	/// <param name="action1">The first action</param>
	/// <param name="action2">The second action</param>
	/// <returns><c>true</c> if the two actions are being pressed at the same time. <c>false</c> otherwise.</returns>
	public static bool GetSimultaneous(string action1, string action2)
	{
		List<Button> a = new List<Button>();
		a.AddRange(KeyControlList.buttons[action1]);
		a.AddRange(JoyControlList.buttons[action1]);

		List<Button> b = new List<Button>();
		b.AddRange(KeyControlList.buttons[action2]);
		b.AddRange(JoyControlList.buttons[action2]);

		foreach(var i in a)
		{
			if (i.CheckPressedWith(b)) return true;
		}

		return false;
	}
}



/// <summary>
/// Control List.
/// 
/// <para>
///		Used to save and to load button layouts for both keyboard controls and button controls.
/// </para>
/// </summary>
[System.Serializable]
public struct ControlList
{
	public List<string> actionList;
	public List<ButtonList> buttonList;

	public ControlList(Dictionary<string, List<Button>> d)
	{
		actionList = DictActionList(d);
		buttonList = DictButtonList(d);
	}

	#region Convert from Dictionary

	/// <summary> Creates a list of strings from the keys of the dictionary. </summary>
	/// <param name="d">Button List Dictionary</param>
	/// <returns>A list of strings for the control list to store.</returns>
	public static List<string> DictActionList(Dictionary<string, List<Button>> d)
	{
		List<string> keyList = new List<string>();
		foreach (var k in d)
		{
			keyList.Add(k.Key);
		}
		return keyList;
	}

	/// <summary> Creates a list of ButtonLists from the values of the dictionary. </summary>
	/// <param name="d">Button List Dictionary</param>
	/// <returns>A list of strings for the control list to store.</returns>
	public static List<ButtonList> DictButtonList(Dictionary<string, List<Button>> d)
	{
		List<ButtonList> keyList = new List<ButtonList>();
		foreach (var k in d)
		{
			keyList.Add(new ButtonList(k.Value));
		}
		return keyList;
	}
	#endregion Convert from Dictionary

	/// <summary> Convert a Control List to a dictionary </summary>
	/// <returns> A dictionary to store in a control scheme </returns>
	public Dictionary<string, List<Button>> ToDict()
	{
		Dictionary<string, List<Button>> d = new Dictionary<string, List<Button>>();

		for (int i = 0; i < actionList.Count; ++i)
		{
			d.Add(actionList[i], buttonList[i].buttons);
		}

		return d;
	}
}

/// <summary>
/// Button List Class
/// <para>
///		a container to store a list of buttons. Used to allow a ControlList to be Serializable
/// </para>
/// </summary>
[System.Serializable]
public struct ButtonList
{
	public List<Button> buttons;

	public ButtonList(List<Button> b)
	{
		buttons = b;
	}
}
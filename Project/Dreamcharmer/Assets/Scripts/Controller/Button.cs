﻿using System.Collections.Generic;
using UnityEngine;

/*
 *      Name:           Button.cs
 *      Author:         Vincent Mills
 *      Version:        0.1.0 (Fixed and documented code.)
 *      Last Modified:  27 June 2017
 */

/// <summary>
/// Button Class:
///		Replaces the <code>Input</code> usage to allow for a buffer system.
///		Buffer system used to allow some give or take to allow simultaneous button presses without sacrificing input lag.
///		Also allows for the usage of names for easy reference.
/// </summary>
[System.Serializable]
public class Button
{
	#region Enumerations

	/// <summary> Button State. Checks what action the given button is taking. </summary>
	public enum State
	{
		/// <summary> Button is being pressed BEFORE buffer period. </summary>
		BUFFER_START,

		/// <summary> Button is being pressed and allows for buttons being pressed at the same time. </summary>
		BUFFER,

		/// <summary> Button has been pressed AFTER buffer period. </summary>
		PRESSED,

		/// <summary> Button is being held until released. </summary>
		HELD,

		/// <summary> Button has been released. </summary>
		RELEASED,

		/// <summary> Button is not being pressed at all. </summary>
		NONE
	}
	#endregion

	#region properties

	public static float inputDelay = 0.025f; //the amount of input delay so the player can press both buttons at the same time.

	public State state;

	public string name;

	public KeyCode key;

	protected float _startTime; //used to check whether or not the buffer is being held for too long.

	#endregion properties

	#region methods

	#region Construction
	/// <summary> Construction. </summary>
	/// <param name="n"> The button's given name.</param>
	/// <param name="k"> The button's given keycode</param>
	public Button(string n, KeyCode k)
	{
		state = State.NONE;
		name = n;
		key = k;
		_startTime = Time.time;
	}
	#endregion Construction

	#region Monobehavior methods
	public void Update()
	{

		bool k = CheckKey();

		switch (state) //go to the state's dedicated method.
		{
			case State.NONE:
				None(k);
				break;
			case State.BUFFER_START:
				BufferStart(k);
				break;
			case State.BUFFER:
				Buffer(k);
				break;
			case State.PRESSED:
				Pressed(k);
				break;
			case State.HELD:
				Held(k);
				break;
			case State.RELEASED:
				Released(k);
				break;
		}
	}

	#endregion Monobehavior methods

	#region Check Simultaneous / Check With

	/// <summary> Checks if ANY two buttons in the list are being pressed/buffered at the same time. </summary>
	/// <param name="b"> The given list of buttons to go through</param>
	/// <returns> <c>true</c> if AT LEAST TWO buttons are being pressed. <c>false</c> otherwise.</returns>
	public static bool CheckPressedSimultaneous(List<Button> b)
	{
		if (b.Count == 0) return false;
		if (b.Count == 1) return true;
		foreach(var i in b)
		{
			if (i.GetKeyDown())
				return i.CheckPressedWith(b);
		}
		return false;
	}

	/// <summary> Checks if both buttons are being pressed. </summary>
	/// <param name="b1">The first button to check.</param>
	/// <param name="b2">The second button to check.</param>
	/// <returns><c>true</c> if both buttons are being pressed. <c>false</c> otherwise.</returns>
	public static bool CheckPressedSimultaneous(Button b1, Button b2)
	{
		return b1.CheckPressedWith(b2);
	}

	/// <summary>
	/// Checks if both the current and the given buttons are being pressed.
	/// </summary>
	/// <param name="b">The button to check.</param>
	/// <returns><c>true</c> if both buttons are being pressed. <c>false</c> otherwise.</returns>
	public bool CheckPressedWith(Button b)
	{
		return GetKeyBuffer() && b.GetKeyBuffer();
	}

	/// <summary> Checks if ANY two buttons in the list are being pressed/buffered at the same time. </summary>
	/// <param name="b"> The given list of buttons to go through</param>
	/// <returns> <c>true</c> if AT LEAST TWO buttons are being pressed. <c>false</c> otherwise.</returns>
	public bool CheckPressedWith(List<Button> b)
	{
		if (!GetKeyBuffer()) return false;
		foreach (var a in b)
		{
			if (a != this && a.GetKeyBuffer())
			{
				a.state = State.HELD;
				state = State.HELD;

				return true;
			}
		}
		return false;
	}
	#endregion Check Simultaneous / Check With


	protected virtual bool CheckKey()
	{
		return Input.GetKey(key);
	}

	#region State Methods
	public void None(bool keyCheck)
	{
		if (keyCheck)
		{
			_startTime = Time.time;
			state = State.BUFFER_START;
		}
	}

	void BufferStart(bool keyPressed)
	{
		state = keyPressed ? State.BUFFER : State.RELEASED;
	}

	public void Buffer(bool keyCheck)
	{
		if (Time.time - _startTime > inputDelay || !keyCheck) //25ms or let go
		{
			state = State.PRESSED;
		}
	}

	public void Pressed(bool keyCheck)
	{
		state = keyCheck ? State.HELD : State.RELEASED;
	}

	public void Held(bool keyCheck)
	{
		if (!keyCheck)
			state = State.RELEASED;
	}

	public void Released(bool keyCheck)
	{
		state = keyCheck ? State.PRESSED : State.NONE;
	}
	#endregion State Methods

	#region GetKey Methods

	/// <summary> Checks if the button has been pressed or is buffered. Same function as Input.GetKeyDown() </summary>
	public bool GetKeyDown()
	{
		return (state == State.BUFFER_START);
	}

	/// <summary> Checks if the button is being buffered. </summary>
	public bool GetKeyBuffer()
	{
		return state == State.BUFFER || GetKeyPressed() || GetKeyDown();
	}

	/// <summary> Checks if the button is being held. Same function as Input.GetKey() </summary>
	public bool GetKey()
	{
		return state == State.HELD || GetKeyBuffer() || GetKeyPressed();
	}

	/// <summary> Checks if the button has been released. Same function as Input.GetKeyUp() </summary>
	public bool GetKeyUp()
	{
		return state == State.RELEASED;
	}

	/// <summary> Checks if the button is just being pressed BUT NOT buffered. </summary>
	public bool GetKeyPressed()
	{
		return state == State.PRESSED;
	}
	#endregion GetKey Methods

	#endregion methods
}

/// <summary>
/// Trigger Class. Inherited from the Button Class.
///		Similar in nature to the button.
///		Uses an axis to check the key instead of just using the button
/// </summary>
[System.Serializable]
public class Trigger : Button
{
	[SerializeField] string axis;

	protected override bool CheckKey()
	{
		return Input.GetAxisRaw(axis) > 0.1; //check if the raw axis is above the threshhold.
	}

	/// <summary> Construction. </summary>
	/// <param name="n">name.</param>
	/// <param name="a">axis name.</param>
	/// <param name="k">Keycode. Leave this as blank.</param>
	public Trigger(string n, string a, KeyCode k = KeyCode.None) : base(n, k)
	{
		key = k;
		name = n;
		axis = a;
	}
}
﻿using System.Collections.Generic;
using UnityEngine;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

/*
 *      Name:           KeyboardControls.cs
 *      Author:         Vincent Mills
 *      Version:        0.1.0 (Documented code.)
 *      Last Modified:  27 June 2017
 */

/// <summary>
/// Keyboard Controls class.
/// 
/// <para>
///		Handler of all of the keyboard controls.
///		
/// </para>
/// </summary>
public static class KeyboardControls
{

	#region Monobehavior

	public static void Update()
	{
		foreach (var k in KeyControlList.buttons)
		{
			foreach (var b in k.Value)
			{
				b.Update();
			}
		}
	}

	#endregion

	#region GetKey Controls
	public static float GetHorizontal()
	{
		foreach (var i in KeyControlList.buttons["Right"])
			if (i.GetKey()) return 1f;

		foreach (var i in KeyControlList.buttons["Left"])
			if (i.GetKey()) return -1f;

		return 0f;
	}

	public static float GetVertical()
	{
		foreach (var i in KeyControlList.buttons["Up"])
			if (i.GetKey()) return 1f;

		foreach (var i in KeyControlList.buttons["Down"])
			if (i.GetKey()) return -1f;

		return 0f;
	}

	public static bool GetKey(string action)
	{
		if (KeyControlList.buttons.ContainsKey(action))
		{
			foreach (var i in KeyControlList.buttons[action])
				if (i.GetKey()) return true;
		}
		return false;
	}

	public static bool GetKeyDown(string action)
	{
		if (KeyControlList.buttons.ContainsKey(action))
		{
			foreach (var i in KeyControlList.buttons[action])
				if (i.GetKeyDown()) return true;
		}
		return false;
	}

	public static bool GetKeyPressed(string action)
	{
		if (KeyControlList.buttons.ContainsKey(action))
		{
			foreach (var i in KeyControlList.buttons[action])
				if (i.GetKeyPressed()) return true;
		}
		return false;
	}

	public static bool GetKeyUp(string action)
	{
		if (KeyControlList.buttons.ContainsKey(action))
		{
			foreach (var i in KeyControlList.buttons[action])
				if (i.GetKeyUp()) return true;
		}
		return false;
	}
	#endregion

	#region Saving and Loading
	public static bool Save()
	{
		BinaryFormatter bf = new BinaryFormatter();
		FileStream file = File.Open(KeyControlList.FilePath(), FileMode.OpenOrCreate);
		var data = new ControlList(KeyControlList.buttons); //changed the data to a generic control list structure.

		bf.Serialize(file, data);
		file.Close();
		Debug.Log("Key Controls Saved!");
		return true;

	}

	public static bool Load()
	{
		if (File.Exists(KeyControlList.FilePath()))
		{
			BinaryFormatter bf = new BinaryFormatter();
			FileStream file = File.Open(KeyControlList.FilePath(), FileMode.Open);
			var data = (ControlList)bf.Deserialize(file);
			file.Close();

			KeyControlList.buttons = data.ToDict();
			Debug.Log("Key Controls Loaded.");
			return true;
		}

		Debug.Log("Now using default values.");
		KeyControlList.buttons = KeyControlList.Default();

		return false;
	}
	#endregion

}

/// <summary>
/// KeyControlList struct.
/// 
/// <para>
///		The current list of controls used for the keyboard. Can change controls to either pre-determined schemes or a fully customized setup.
/// </para>
/// 
/// </summary>
public struct KeyControlList
{
	/// <summary> The buttons used to use for Keyboard Controls. This will be the list that will be saved to/loaded from the ControlList class. </summary>
	public static Dictionary<string, List<Button>> buttons = Default();

	/// <summary> Gets the intended file path for keycontrols. </summary>
	/// <returns>the file path for key controls.</returns>
	public static string FilePath()
	{
		return Application.persistentDataPath + "/keycontrols.dat";
	}

	#region Control Schemes
	public static Dictionary<string, List<Button>> Default()
	{
		var c = new Dictionary<string, List<Button>>();
		c.Add("Up", new List<Button>() { new Button("Up", KeyCode.W), new Button("Up", KeyCode.UpArrow) });
		c.Add("Down", new List<Button>() { new Button("Down", KeyCode.S), new Button("Down", KeyCode.DownArrow) });
		c.Add("Left", new List<Button>() { new Button("Left", KeyCode.A), new Button("Left", KeyCode.LeftArrow) });
		c.Add("Right", new List<Button>() { new Button("Right", KeyCode.D), new Button("Right", KeyCode.RightArrow) });
		c.Add("Jump", new List<Button>() { new Button("Jump", KeyCode.Space) });
		c.Add("Attack", new List<Button>() { new Button("Attack", KeyCode.H) });
		c.Add("Red", new List<Button>() { new Button("Red", KeyCode.J) });
		c.Add("Yellow", new List<Button>() { new Button("Yellow", KeyCode.K) });
		c.Add("Green", new List<Button>() { new Button("Green", KeyCode.L) });
		c.Add("Blue", new List<Button>() { new Button("Blue", KeyCode.Semicolon) });
		c.Add("Pause", new List<Button>() { new Button("Pause", KeyCode.P) });

		return c;
	}
	#endregion Control Schemes

	/// <summary> Change either primary or secondary controls for actions. </summary>
	/// <param name="action">the inteded action</param>
	/// <param name="button">intended button to change to.</param>
	/// <param name="index">0 for primary button, 1 for secondary.</param>
	/// <returns> <c>true</c> if the control has been changed. <c>false</c> otherwise.</returns>
	public static bool ChangeControl(string action, Button button, int index = 0)
	{
		if (buttons.ContainsKey(action))
		{
			switch(index)
			{
				case 0:
					buttons[action][0] = button;
					return true;
				case 1:
					if (buttons[action].Count > 1)
						buttons[action][1] = button;
					else
						buttons[action].Add(button);
					return true;
				default:
					return false;
			}
		}
		return false;
	}
}
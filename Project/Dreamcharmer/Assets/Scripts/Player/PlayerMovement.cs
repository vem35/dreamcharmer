﻿using UnityEngine;

/*
 *      Name:           PlayerMovement.cs
 *      Author:         Vincent Mills
 *      Version:        0.1.2 (Documented code.)
 *      Last Modified:  27 June 2017
 */

/// <summary> Basic Player movement script. </summary>
public class PlayerMovement : MonoBehaviour {

	#region Enumerations
	enum MoveState
	{
		MOVE,
		DASH,
		NONE
	}
	#endregion Enumerations

	#region variables

	#region public

	public Animator animator;


	[SerializeField] float maxSpeed;
	[SerializeField] GameObject dashTrigger;
	[SerializeField] float jumpForce;
	[SerializeField] float jumpVelocity;
	float _jumpstorage;

    [SerializeField] float raycastThreshhold; //the threshhold for raycasting
    [SerializeField] float moveForce;
	[SerializeField] LayerMask whatIsGround;

	[SerializeField] bool _leftFace;

	/// <summary> Get Only. Get whether or not the player is facing left. (false by default) </summary>
	public bool leftFace { get { return _leftFace; } }

	public bool control;

    [SerializeField] GravitySettings gravitySettings;
	[SerializeField] float terminalVelocity;
	public Vector2 velocity { get { return _rb.velocity; } }

	MoveState _moveState;
	public GameObject trail;
	Vector2 _dashVelocity;

    #endregion public

    #region private
    /// <summary> The Player's Rigidbody </summary>
    private Rigidbody2D _rb;

    private bool jump;

	public bool grounded { get { return ground; } }
	public GameObject ground
	{
		get
		{
			var a = Physics2D.Raycast(transform.position, Vector3.down, raycastThreshhold, whatIsGround);
			return a ?  a.collider.gameObject : null;
		}
	}

	private bool jumpHControl;

	#endregion private

	#endregion

	#region Monodevelop

	// Use this for initialization
	void Start()
	{
		_rb = gameObject.GetComponent<Rigidbody2D>(); //get the rigidbody component
		jumpHControl = true;
	}

	// Update is called once per frame
	void Update()
	{
		if (control)
		{
			if (Controls.GetKeyDown("Jump") && grounded)
			{
				if (Controls.GetAxis(Direction.VERTICAL) < 0 && ground.GetComponent<BuildingFallThrough>())
					ground.GetComponent<BuildingFallThrough>().Fall();
				else
					jump = true; //jump if press the jump button and not falling through a platform.
			}
		}
		animator.SetFloat("hspeed", Mathf.Abs(_rb.velocity.x));
		animator.SetFloat("vspeed", _rb.velocity.y);
		animator.SetBool("grounded", grounded);
	}


	void FixedUpdate()
	{

		switch (_moveState)
		{
			case MoveState.MOVE:
				if (jump)
				{
					JumpStart();
				}

				if (control)
				{

					var f = Controls.GetAxis(Direction.HORIZONTAL);

					if (f > 0 && leftFace)
						Flip();
					else if (f < 0 && !leftFace)
						Flip();

					_rb.velocity = new Vector2(Mathf.Lerp(_rb.velocity.x, grounded || jumpHControl ? maxSpeed * f : 0f, 0.2f), _rb.velocity.y);
					if (_rb.velocity.y > 0 && Controls.GetKeyUp("Jump"))
						_rb.velocity = new Vector2(_rb.velocity.x, _rb.velocity.y / 2);

				}

				if (_rb.velocity.y < 0)
				{
					if (_rb.velocity.y < -terminalVelocity)
						_rb.velocity = new Vector2(_rb.velocity.x, -terminalVelocity);
					_rb.gravityScale = gravitySettings.gravityScale * gravitySettings.fallMultiplier;
				}
				else _rb.gravityScale = gravitySettings.gravityScale;
				break;
			case MoveState.DASH:
				_rb.velocity = _dashVelocity;
				return;
			default:
				return;
		}
	}

	public void OnTimerEnd(Timer t)
	{
		if (t.name == "dash")
		{
			_moveState = MoveState.MOVE;
			t.state = Timer.TimerState.PAUSE;
			_rb.gravityScale = gravitySettings.gravityScale;
			_rb.velocity = new Vector2(_rb.velocity.x, _rb.velocity.y / 3f);
			t.ResetTimer();
			if (dashTrigger) dashTrigger.SetActive(false);
		}
	}
	#endregion

	/// <summary>
	/// Flip function. Flip the player.
	/// </summary>
	public void Flip()
	{
		_leftFace = !_leftFace;
		transform.localScale = new Vector3(-transform.localScale.x, transform.localScale.y, transform.localScale.z);
	}


	public void StopMotion(int invunerable = 1)
	{
		control = false;
		_rb.velocity = Vector2.zero;
		_moveState = MoveState.NONE;
		if (dashTrigger) dashTrigger.SetActive(false);
		if(invunerable > 0)
			GetComponent<Player>().iTime = 99;
	}

	public void ContinueMotion()
	{
		control = true;
		_moveState = MoveState.MOVE;
		GetComponent<Player>().iTime = 0f;
		_rb.gravityScale = gravitySettings.gravityScale;
		_rb.velocity = new Vector2(_rb.velocity.x, _rb.velocity.y / 3f);
		if (dashTrigger) dashTrigger.SetActive(false);
	}

	public void Jump()
	{
		_rb.velocity = new Vector2(_jumpstorage, jumpVelocity);
		control = true;
	}

	public void JumpStart(bool resetY = false, float jumpMultiplier = 1f, float horizontalMultiplier = 0f, bool control = true, bool resetX = false)
	{
		if (resetY)
			_rb.velocity = new Vector2(_rb.velocity.x, 0f);
		if (resetX)
			_rb.velocity = new Vector2(0f, _rb.velocity.y);
		animator.SetTrigger("jump");
		_jumpstorage = _rb.velocity.x;
		_rb.velocity = Vector2.zero;
		this.control = false;
		jump = false;
		Jump();
	}

	public void Dash(float horizontalSpeed, float verticalSpeed = 0f, bool control = false, bool setInvunerability = true)
	{
		_rb.velocity = new Vector2(horizontalSpeed, verticalSpeed);
		_dashVelocity = new Vector2(horizontalSpeed, verticalSpeed);
		if (setInvunerability)
		{
			GetComponent<TimerList>()["dash"].state = Timer.TimerState.PLAY;
			GetComponent<Player>().iTime = GetComponent<TimerList>()["dash"].duration;
		}
		_rb.gravityScale = 0f;
		_moveState = MoveState.DASH;
		if (dashTrigger) dashTrigger.SetActive(true);
	}

}

[System.Serializable]
public struct GravitySettings
{
    public int gravityScale;
    public int fallMultiplier;

    public GravitySettings (int scale, int mult)
    {
        gravityScale = scale;
        fallMultiplier = mult;
    }
}
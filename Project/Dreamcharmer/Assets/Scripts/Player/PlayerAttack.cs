﻿using UnityEngine;

public class PlayerAttack : MonoBehaviour {

	[SerializeField] int _power;
	public int power { get { return _power; } set { _power = value; } }

}

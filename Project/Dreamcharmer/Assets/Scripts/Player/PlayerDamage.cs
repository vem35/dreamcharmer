﻿using UnityEngine;

/*
 *		Name:			PlayerDamage.cs
 *		Author:			Vincent Mills
 *		Version:		0.1.0 (Created STILL and MOVE_TO states.)
 *		Last Modified:	16 April 2017
 */


/// <summary>
/// Player Damage class. Checks if the player is colliding with the 
/// </summary>
public class PlayerDamage : MonoBehaviour {
	[SerializeField] Animator _anim;
	[SerializeField] Player player;
	[SerializeField] int invunerabilityFrames;

	private void Damage(int damage)
	{
		player.Damage(damage);
		if (player.stats.health <= 0)
			player.Die();

		_anim.SetBool("blink", true);
		player.iTime = invunerabilityFrames / 60f;
	}


	private void OnTriggerEnter2D(Collider2D collision)
	{
		if (collision.tag == "EnemyBullet" && !player.invunerableCheck)
		{
			if(collision.GetComponent<EnemyAttack>() == null)
				Damage(collision.transform.parent.GetComponent<EnemyAttack>() == null ? 1 : collision.transform.parent.GetComponent<EnemyAttack>().damage);
			else
				Damage(collision.transform.GetComponent<EnemyAttack>() == null ? 1 : collision.transform.GetComponent<EnemyAttack>().damage);
		}
	}

	private void OnTriggerStay2D(Collider2D collision)
	{
		if(collision.tag == "SustainedDamage" && !player.invunerableCheck)
		{
			if (collision.GetComponent<EnemyAttack>() == null)
				Damage(collision.transform.parent.GetComponent<EnemyAttack>() == null ? 1 : collision.transform.parent.GetComponent<EnemyAttack>().damage);
			else
				Damage(collision.transform.GetComponent<EnemyAttack>() == null ? 1 : collision.transform.GetComponent<EnemyAttack>().damage);
		}
	}
}

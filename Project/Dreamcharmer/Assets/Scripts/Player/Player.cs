﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

/*
 *      Name:           Player.cs
 *      Author:         Vincent Mills
 *      Version:        0.1.0 (Documented code.)
 *      Last Modified:  27 June 2017
 */

/// <summary> Player script. Singleton design. Handles all of the statistics and needs of the player character.  </summary>
/// <remarks> Singleton Class. </remarks>
[RequireComponent(typeof(PlayerMovement))]
public class Player : MonoBehaviour {
	
	#region variables

	/// <summary> The main player object. Use this to keep track of the player</summary>
	public static Player main;

	/// <summary> The center of the player. Use this for shooting and everything else. </summary>
	[Header("Components")]
	[SerializeField] Transform pivot;
	[SerializeField] Transform center;
	[SerializeField] Animator _anim;

	private GameObject trailCurrent;

	PlayerMovement _move;

	/// <summary> Get only. The player movement component </summary>
	public PlayerMovement move { get { return _move; } }

    [SerializeField] GameObject curevfx;

    [Header("Dreams")]
	[SerializeField] PlayerStats _stats;
	[SerializeField] PlayerInventory _inventory;
	/// <summary> Get only. The player's statistics to use later on. </summary>
	public PlayerStats stats { get { return _stats; } }

	[System.NonSerialized] public int boosts = 0;

	[Header("Properties")]
	public float iTime;
	[SerializeField] bool _invunerable;
	public bool invunerableCheck
	{
		get
		{
			return _invunerable || iTime > 0f;
		}
	}

	public Interactable interactable;

	#endregion

	#region Methods

	#region Monodevelop
	void OnTriggerEnter2D(Collider2D c)
	{
		if (c.tag == "Dream")
		{
			AddDream(c.GetComponent<Dream>().stats);
			c.gameObject.SetActive(false);
		}
		else if (c.tag == "Interact")
		{
			Debug.Log(c.tag);
			interactable = c.GetComponent<Interactable>();
		}
	}

	private void OnDisable()
	{
		Debug.Log("what");
	}

	private void OnTriggerExit2D(Collider2D collision)
	{
		if (collision.tag == "Interact" && interactable == collision.GetComponent<Interactable>())
			interactable = null;
	}

	private void Awake()
	{
		main = this;
		_move = GetComponent<PlayerMovement>();
		_move.GetComponent<Rigidbody2D>().sleepMode = RigidbodySleepMode2D.NeverSleep;
	}

	private void Start()
	{
		interactable = null;
		stats.Load();
	}

	private void Update()
	{
		if (boosts > 0f && move.grounded)
			boosts = 0;

		move.animator.SetBool("invunerable", invunerableCheck);
		if (Time.timeScale > 0f)
			ScaledUpdate();
	}

	/// <summary> Update only if the time scale is greater than 0 </summary>
	private void ScaledUpdate()
	{
		if (move.control)
		{
			Shooting();
			if (interactable && Controls.GetAxis(Direction.VERTICAL) > 0)
			{
				interactable.Interact();
			}
		}
		if (iTime > 0f)
			iTime -= Time.deltaTime;
	}
	#endregion

	#region Actions
	void AddDream(DreamStat d)
    {
        switch(d.dreamColor)
        {
            case DreamColor.BLUE:
                _stats.blueDreams += d.amount;
                break;
            case DreamColor.RED:
                _stats.redDreams += d.amount;
                break;
			case DreamColor.YELLOW:
				_stats.yellowDreams += d.amount;
				break;
            default:
                _stats.greenDreams += d.amount;
                break;
        }
    }

	private void Shooting()
	{
		if ((_stats[DreamColor.YELLOW] > 0 && _stats[DreamColor.GREEN] > 0) && Controls.GetSimultaneous("Yellow", "Green"))
			UseCombo(DreamColor.YELLOW, DreamColor.GREEN);
		else if (Controls.GetSimultaneous("Red", "Blue") && _stats[DreamColor.RED] > 0 && _stats[DreamColor.BLUE] > 0)
			UseCombo(DreamColor.RED, DreamColor.BLUE);
		else if (Controls.GetKeyPressed("Red") && _stats[DreamColor.RED] > 0)
			Use(DreamColor.RED);
		else if (Controls.GetKeyPressed("Yellow") && _stats[DreamColor.YELLOW] > 0)
			Use(DreamColor.YELLOW);
		else if (Controls.GetKeyPressed("Green") && _stats[DreamColor.GREEN] > 0 && move.grounded && _stats.health < _stats.healthMax)
			_anim.SetTrigger("cure"); //start the cure animation. The cure animation will use the green power.
		else if (Controls.GetKeyPressed("Blue") && _stats[DreamColor.BLUE] > 0)
			Use(DreamColor.BLUE);
		else if (Controls.GetKeyDown("Attack"))
			Attack();
	}
	#endregion Actions

	#region Attack and Use
	/// <summary> Do a basic attack. </summary>
	private void Attack()
	{
		_inventory.CreateBasicAttack(pivot.position, transform.rotation, _move.leftFace, pivot);
		_anim.SetTrigger("shoot");
	}

	/// <summary> Use a combination power based on the two dreams. </summary>
	/// <param name="d1">The first dream</param>
	/// <param name="d2">The second dream</param>
	/// <param name="swap"><c>true</c> if you can swap between them. <c>false</c> otherwise.</param>
	private void UseCombo(DreamColor d1, DreamColor d2, bool swap = true)
	{
		DreamAnimations.main.Glow(d1);
		DreamAnimations.main.Glow(d2);
		switch(d1)
		{
			case DreamColor.YELLOW:
				if (_stats.yellowDreams > 0)
				{
					switch (d2)
					{
						case DreamColor.GREEN:
							//use the protect ability
							if (_stats.greenDreams > 0)
							{
								iTime = 4f;
								_anim.SetTrigger("bubble");
								stats.greenDreams--;
								stats.yellowDreams--;
							}
							break;
					}
				}
				return;
			case DreamColor.RED:
				if(_stats.redDreams > 0)
				{
					switch (d2)
					{
						case DreamColor.BLUE:
							//use the chain lightning attack
							if(_stats.blueDreams > 0)
							{
								GameObject a = _inventory.CreateLightning(pivot.position, transform.rotation, _move.leftFace);
								a.GetComponent<Rigidbody2D>().velocity = new Vector2(GetComponent<PlayerMovement>().leftFace ? -_inventory.fireballSpeed : _inventory.fireballSpeed, 0f);
								_anim.SetTrigger("shoot");
								stats.redDreams--;
								stats.blueDreams--;
							}
							break;
					}
				}
				return;
			default:
				if(swap)
					UseCombo(d2, d1, false); //try it again but swap the colors.
				return;
		}
	}

	/// <summary> Use a singular dream. </summary>
	/// <param name="d">INtended dream color.</param>
	private void Use(DreamColor d)
	{
		DreamAnimations.main.Glow(d);
		if (d == DreamColor.RED) //shoot a fireball when using a red dream
		{
			GameObject a = _inventory.CreateFireball(pivot.position, transform.rotation, _move.leftFace);
			a.GetComponent<Rigidbody2D>().velocity = new Vector2(GetComponent<PlayerMovement>().leftFace ? -_inventory.fireballSpeed : _inventory.fireballSpeed, 0f);
			_anim.SetTrigger("shoot");
			stats.redDreams--;
		}
		if(d == DreamColor.YELLOW) //dash when using a yellow dream
		{
			if(boosts < _inventory.boostMaximum)
			{
				Vector2 dashVel = _inventory.GetDashDirection(Controls.GetAxis(Direction.HORIZONTAL), Controls.GetAxis(Direction.VERTICAL), move.leftFace) * _inventory.dashSpeed;
				Debug.Log(dashVel);
				move.Dash(dashVel.x, dashVel.y);
				stats.yellowDreams--;
				trailCurrent = Instantiate(move.trail, center) as GameObject;
				Invoke("TrailLeave", 0.25f);
				++boosts;
				return;
			}
		}
		if(d == DreamColor.GREEN) //cure yourself when the animation calls for it
		{
			if (stats.health == stats.healthMax)
				return;
			stats.health += _inventory.Heal(stats.health, stats.healthMax);
			stats.greenDreams--;

            GameObject a = Instantiate(curevfx, transform.position + new Vector3(0,1.5f,0), transform.rotation);
			a.transform.SetParent(pivot);
            Destroy(a, 2);

        }
		if(d == DreamColor.BLUE) //use the reflect power when using a blue dream
		{
			GameObject a = _inventory.CreatePellet(pivot.position, transform.rotation, move.leftFace);
			_inventory.Shoot(a, GetComponent<PlayerMovement>().leftFace);
			_anim.SetTrigger("shoot");
			stats.blueDreams--;
		}
	}

	/// <summary> Break the trail. </summary>
	public void TrailLeave()
	{
		if (trailCurrent)
			trailCurrent.transform.parent = null;
	}
	#endregion Attack and Use

	public void Die()
	{
		_stats.SaveFullHealth();
		_anim.SetTrigger("death");
		_move.control = false;
		GetComponent<Rigidbody2D>().velocity = new Vector2();
		Invoke("ResetScene", 1.5f);
	}

	
	public void ResetScene()
	{
		SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
	}

	public float GetRotation(float horizontal, float vertical, bool leftCheck)
	{
		Vector2 v= new Vector2(horizontal, vertical);
		if (v == Vector2.zero)
			v = new Vector2(leftCheck ? -1 : 1, 0);
		return Vector2.Angle(Vector2.right, v);
	}

	public void Damage(int damage)
	{
		stats.health -= damage;
	}


	#endregion Methods
}

/// <summary>
/// PlayerStats class.
/// <para>
///		Keeps track of all basic statistics for the player.
/// </para>
/// </summary>
[System.Serializable]
public class PlayerStats
{

	#region Variables

	public static PlayerStats main = null;

	[Header("Health")]
	public int health;
	public int healthMax;

	[Header("Dreams")]
	public int redDreams;
	public int yellowDreams;
	public int greenDreams;
	public int blueDreams;



	#endregion Variables

	#region Amount Functions
	public int this[DreamColor c]
	{
		get
		{
			return GetAmount(c);
		}
		set
		{
			SetAmount(c, value);
		}
	}

	private int GetAmount(DreamColor c)
	{
		switch (c)
		{
			case DreamColor.BLUE:
				return blueDreams;
			case DreamColor.RED:
				return redDreams;
			case DreamColor.GREEN:
				return greenDreams;
			case DreamColor.YELLOW:
				return yellowDreams;
			default:
				return new int();
		}
	}

	private void SetAmount(DreamColor c, int value)
	{
		switch (c)
		{
			case DreamColor.BLUE:
				blueDreams = value;
				break;
			case DreamColor.RED:
				redDreams = value;
				break;
			case DreamColor.GREEN:
				greenDreams = value;
				break;
			case DreamColor.YELLOW:
				yellowDreams = value;
				break;
		}
	}
	#endregion Amount Functions

	#region Copy Function
	public void Copy(PlayerStats s)
	{
		health = s.health;
		healthMax = s.healthMax;
		redDreams = s.redDreams;
		yellowDreams = s.yellowDreams;
		greenDreams = s.greenDreams;
		blueDreams = s.blueDreams;
	}

	public PlayerStats(PlayerStats s)
	{
		Copy(s);
	}
	#endregion

	#region Saving and Loading
	/// <summary> Save the health to the main for future use. </summary>
	public void Save()
	{
		main = new PlayerStats(this);
	}

	public void Load()
	{
		if (main != null)
		{
			Copy(main);
		}
	}

	/// <summary> Set the health of the main to full health if applicable. </summary>
	public void SaveFullHealth()
	{
		if (main != null)
			main.health = main.healthMax;
	}
	#endregion
}

/// <summary>
/// Player Inventory Class.
///	
/// <para>
///		The list of all of the additional powers for Lowell
/// </para>
/// </summary>
[System.Serializable]
public class PlayerInventory
{
	#region Basic Attack
	[System.NonSerialized]
	public List<GameObject> attackList = new List<GameObject>();
	[Header("Basic Attack")]
	public GameObject basicAttack;
	[SerializeField] float scale = 0.5f;

	public GameObject CreateBasicAttack(Vector3 position, Quaternion rotation, bool leftFace = false, Transform parent = null)
	{
		int activeList = 0;
		foreach (var o in attackList)
		{
			if (!o.activeSelf)
			{
				o.transform.position = position;
				o.transform.rotation = rotation;
				o.transform.localScale = new Vector3(leftFace ? scale : scale, scale, 1f);
				o.SetActive(true);
				if (parent != null)
				{
					o.transform.SetParent(parent);
					o.transform.localPosition = Vector3.zero;
					o.transform.localRotation = Quaternion.identity;
				}

				return o;
			}
			else
			{
				if(++activeList >= 2)
					return null;
			}
		}
		GameObject a = Object.Instantiate(basicAttack, position, rotation) as GameObject;
		a.transform.localScale = new Vector3(leftFace ? -scale : scale, scale, 1f);
		if (parent != null)
			a.transform.SetParent(parent);
		attackList.Add(a);
		return a;
	}
	#endregion Basic Attack

	#region Fireball
	[System.NonSerialized] public List<GameObject> fireballList = new List<GameObject>();
	[Header("Fireball")]
	public GameObject fireball;
	public int damage;
	public float fireballSpeed;

	public GameObject CreateFireball(Vector3 position, Quaternion rotation, bool leftFace = false)
	{
		foreach(var o in fireballList)
		{
			if(!o.activeSelf)
			{
				o.transform.position = position;
				o.transform.rotation = rotation;
				o.transform.localScale = new Vector3(leftFace ? -1 : 1, leftFace ? -1 : 1, 1f);
				o.SetActive(true);
				return o;
			}
		}
		GameObject a =  Object.Instantiate(fireball, position, rotation) as GameObject;
		a.transform.localScale = new Vector3(leftFace ? -1 : 1, leftFace ? -1 : 1, 1f);
		fireballList.Add(a);
		return a;
	}
	#endregion Fireball

	#region Dash
	[Header("Dash")]
	public float boostMaximum;
	public float dashSpeed;
	public Vector2 GetDashDirection(float horizontalMotion, float verticalMotion, bool leftFace, float threshhold = 0.1f)
	{
		Vector2 a = new Vector2(Mathf.Abs(horizontalMotion) > threshhold ? horizontalMotion : 0f, Mathf.Abs(verticalMotion) > threshhold ? verticalMotion : 0f);
		if (a.magnitude > 0f)
			return a.normalized;
		return new Vector2(leftFace ? -1f : 1f, 0f);
	}
	#endregion Dash

	#region Heal
	[Header("Heal")]
	public bool healOnPercent;
	public float healPercentage;
	public int hpGain;

	public int Heal(int health, int healthMax)
	{
		if(healOnPercent)
		{
			int gain = (int)(healthMax * healPercentage);
			if (health + gain > healthMax)
				return healthMax - health;
			return gain;
		}
		if (health + hpGain > healthMax)
			return healthMax - health;
		return hpGain;
	}
	#endregion Heal

	#region Close Range Attack
	[System.NonSerialized]
	public List<GameObject> pelletList = new List<GameObject>();
	[Header("Close Range")]
	public GameObject closeRangePellet;
	public float pelletSpeed;

	public GameObject CreatePellet(Vector3 position, Quaternion rotation, bool leftFace = false)
	{
		foreach (var o in pelletList)
		{
			if (!o.activeSelf)
			{
				o.transform.position = position;
				o.transform.rotation = rotation;
				o.transform.localScale = new Vector3(Mathf.Abs(o.transform.localScale.x) * (leftFace ? -1 : 1), o.transform.localScale.y, 1f);
				o.SetActive(true);
				return o;
			}
		}
		GameObject a = Object.Instantiate(closeRangePellet, position, rotation) as GameObject;
		a.transform.localScale = new Vector3(Mathf.Abs(a.transform.localScale.x) * (leftFace ? -1 : 1), a.transform.localScale.y, 1f);
		pelletList.Add(a);
		return a;
	}

	public void Shoot(GameObject pellet, bool leftFace)
	{
		pellet.GetComponent<Rigidbody2D>().velocity = new Vector2(leftFace ? -pelletSpeed : pelletSpeed, 0f);
	}
	#endregion Shotgun

	#region Chain Lightning
	[System.NonSerialized] public List<GameObject> chainList = new List<GameObject>();
	[Header("Chain Lightning")]
	public GameObject chainLightning;
	public float chainLightningSpeed;

	public GameObject CreateLightning(Vector3 position, Quaternion rotation, bool leftFace = false)
	{
		foreach (var o in chainList)
		{
			if (!o.activeSelf)
			{
				o.transform.position = position;
				o.transform.rotation = rotation;
				o.transform.localScale = new Vector3(leftFace ? -1 : 1, leftFace ? -1 : 1, 1f);
				o.SetActive(true);
				return o;
			}
		}
		GameObject a = Object.Instantiate(chainLightning, position, rotation) as GameObject;
		a.transform.localScale = new Vector3(leftFace ? -1 : 1, leftFace ? -1 : 1, 1f);
		chainList.Add(a);
		return a;
	}
	#endregion Chain Lightning
}
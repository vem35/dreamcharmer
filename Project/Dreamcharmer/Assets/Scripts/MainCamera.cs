﻿using UnityEngine;

/*
 *		Name:			MainCamera.cs
 *		Author:			Vincent Mills
 *		Version:		0.1.1 (Fixed a bug where the camera would move along the Z-axis unnecesarily when moving.)
 *		Last Modified:	17 April 2017
 */

/// <summary>
/// Main Camera Class:
///		<para>Can swap between following an object, staying still, and moving to a position.</para>		
/// </summary>
public class MainCamera : MonoBehaviour {

	#region Variables
	/// <summary>
	/// The possible states of the camera.
	/// </summary>
	public enum CameraState
	{
		FOLLOW,
		STILL,
		MOVE_TO
	}

	/// <summary>
	/// The camera's state:
	/// 
	/// <para>Use FOLLOW to have the camera follow a gameobject immediately.</para>
	/// <para>Use STILL when you want to manually move the camera</para>
	/// <para>Use MOVE_TO when you want it to move to a spot at a certain speed.</para>
	/// 
	/// </summary>
	public CameraState state;

	#region Following
	[Header("Following")]
    
	[SerializeField] Transform following; //the transform that the camera follows

	[SerializeField] Rect _border; //the border to follow the player around in world space

	/// <summary> Encapsulated. The border to stop following the player </summary>
	public Rect border
	{
		get
		{
			return _border;
		}
		set
		{
			_border = value;
		}
	}

	#endregion

	#region MOVE_TO

	[Header("Move To")]
	[SerializeField] Vector2 _destination; //the intended destination to move to

	Vector3 destination //the destination to move to, but where its position stays at the camera's position.
	{
		get
		{
			return new Vector3(_destination.x, _destination.y, transform.position.x);
		}
	}

	[SerializeField] float moveSpeed;
	
	/// <summary> <c>true</c> if camera moves in scaled time. <c>false</c> if move in unscaled time. </summary>
	public bool scaledTime;

	[SerializeField] CameraState moveEndState;


	#endregion

	#region Components
	private Camera _camera;
	#endregion Components

	#endregion Variables

	#region Methods

	#region ChangeStates

	#region Follow

	/// <summary>
	/// Change state to follow the game object.
	/// </summary>
	/// <param name="g">The intended <c>GameObject</c> to follow.</param>
	public void Follow(GameObject g)
	{
		Follow(g, border);
	}

	/// <summary>
	/// Change state to follow the game object.
	/// </summary>
	/// <param name="g">The intended <c>GameObject</c> to follow.</param>
	/// <param name="b">The intended <c>Rect</c> to border.</param>
	public void Follow(GameObject g, Rect b)
	{
		state = CameraState.FOLLOW;
		following = g.transform;
		border = b;
	}
	#endregion

	#region Still

	/// <summary> Stop moving the camera and stay still for free movement. </summary>
	public void Stop()
	{
		state = CameraState.STILL;
	}
	#endregion Still

	#region Move
	/// <summary> Change the state to move to a destination. </summary>
	/// <param name="position">The intended destination to move the camera to</param>
	/// <param name="scaled">Whether or not the camera moved in scaled time</param>
	/// <param name="c">cThe state to change to when the camera finished moving</param>
	public void MoveTo(Vector2 position, bool scaled = true, CameraState c = CameraState.STILL)
	{
		MoveTo(position, moveSpeed, scaled, c);
	}

	/// <summary> Change the state to move to a destination. </summary>
	/// <param name="speed">ath</param>
	/// <param name="position">The intended destination to move the camera to</param>
	/// <param name="scaled">Whether or not the camera moved in scaled time</param>
	/// <param name="c">cThe state to change to when the camera finished moving</param>
	public void MoveTo(Vector2 position, float speed, bool scaled = true, CameraState c = CameraState.STILL)
	{
		_destination = position;
		moveSpeed = speed;
		scaledTime = scaled;
		moveEndState = c;
	}
	#endregion Move

	#endregion ChangeStates

	#region State Functions

	void Readjust()
	{
		float width = _camera.orthographicSize * _camera.aspect; //calculate the width of the camera based on aspect ratio
		float height = _camera.orthographicSize;

		Vector3 position;

		position.x = Mathf.Min(Mathf.Max(following.position.x, border.xMin + width), border.xMax - width); //position the camera so the horizontal borders don't go past the regular borders of the screen
		position.y = Mathf.Min(Mathf.Max(following.position.y, border.yMin + height), border.yMax - height); //position the camera so the vertical borders don't go past the regular borders of the screen
		position.z = transform.position.z;

		transform.position = position;
	}

	void Move()
	{
		float spd = moveSpeed * (scaledTime ? Time.deltaTime : Time.unscaledDeltaTime);

		if(spd > ((Vector2)(destination - transform.position)).magnitude)
		{
			Vector2 _d = destination;
			Vector3 d = new Vector3(_d.x, _d.y, transform.position.z);
			transform.position = d;
			state = moveEndState;
		}
		else
		{
			Vector2 direction = (destination - transform.position);
			transform.Translate(direction.normalized * spd);
		}
	}
	#endregion State Functions

	#region Monodevelop
	void Awake()
    {
        _camera = GetComponent<Camera>();
    }

    void Start () {
		if (following == null) enabled = false;
		if (state == CameraState.FOLLOW)
			Readjust();
	}

	void Update()
	{
		if (state == CameraState.MOVE_TO)
			Move();
	}

	void FixedUpdate () {

		if(state == CameraState.FOLLOW)
			Readjust();
	}
	#endregion

	#endregion

}
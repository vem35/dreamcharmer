﻿using UnityEngine;

/*
 *  Name:           VisionTigger.cs
 *  Author:         Josh Megahan
 *  Version:        0.0.4
 *  Last Modified:  14 May 2017
 */

/// <summary> Monster Vision. If player is inside hitbox, monster sends raycasts to confirm line of sight </summary>
public class VisionTrigger : MonoBehaviour {
    private bool _seePlayer = false;
    private Vector2 _lastPlayerPos;
    private LayerMask player;
    private LayerMask ground;
    private LayerMask mask;

    public bool seePlayer {
        get {
            return _seePlayer;
        }
    }

    public Vector2 lastPlayerPos {
        get {
            return _lastPlayerPos;
        }
    }

    private void Awake() {
        player = (1 << LayerMask.NameToLayer("Player"));
        ground = (1 << LayerMask.NameToLayer("Ground"));
        mask = ground | player;
    }

    private void OnTriggerStay2D(Collider2D col) {
        // If player is in vision hitbox check if raycast connects
        if (col.CompareTag("Player")) {
            Vector3 pos = transform.position;
            Vector2 dir = (col.transform.position + new Vector3(0f, 0.5f, 0f)) - pos;
            RaycastHit2D hit = Physics2D.Raycast(pos, dir, Mathf.Infinity, mask.value);

            Debug.DrawRay(pos, dir);


            // 
            _seePlayer = false;
            if (hit.collider != null && hit.collider.CompareTag("Player")) {
                _seePlayer = true;
                _lastPlayerPos = col.transform.position;
            }
        }
    }

    // If outside of 
    private void OnTriggerExit2D(Collider2D col) {
        if (col.gameObject.CompareTag("Player")) {
            _seePlayer = false;
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Fireball))]
[RequireComponent(typeof(EnemyAttack))]
public class EnemyFireball : MonoBehaviour {
	
	enum FireballState
	{
		ENEMY,
		PLAYER
	}

	[SerializeField] FireballState state;
	FireballState defaultState;
	[SerializeField] int damage;

	[Header("Player")]
	public string bulletTag;
	PlayerAttack _fireball;
	public string bulletMask;
	public int _playerMask { get { return LayerMask.NameToLayer(bulletMask); } }

	[Header("Enemy")]
	public string enemyTag;
	EnemyAttack _enemy;
	public string enemyBulletMask;
	private int _enemyMask { get { return LayerMask.NameToLayer(enemyBulletMask); } }

    [Header("Reflect")]
    [SerializeField] RuntimeAnimatorController reflectController;

    private Animator _anim;

	private void Awake()
	{
        _anim = GetComponent<Animator>();
		_fireball = GetComponent<Fireball>();
		_enemy = GetComponent<EnemyAttack>();
	}

	private void Start()
	{
		_enemy.SetDamage(damage);
		_fireball.power = damage;
	}

	private void OnEnable()
	{
		ChangeState(state);
		defaultState = state;
	}

	private void OnDisable()
	{
		ChangeState(defaultState);
	}

	void ChangeState(FireballState s)
	{
		switch (s)
		{
			case FireballState.PLAYER:
				gameObject.layer = _playerMask;
				gameObject.tag = bulletTag;
				if (_enemy)
					_enemy.enabled = false;
				if (_fireball)
					_fireball.enabled = true;
				break;
			case FireballState.ENEMY:
				gameObject.layer = _enemyMask;
				gameObject.tag = enemyTag;
				if (_enemy)
					_enemy.enabled = true;
				if (_fireball)
					_fireball.enabled = false;
				break;
		}
		state = s;
	}

	public void Reflect()
	{
        _anim.runtimeAnimatorController = reflectController;
        _fireball = GetComponent<Fireball>();
        if (GetComponent<ProjectileHoming>() != null) {
            GetComponent<ProjectileHoming>().enabled = false;
        }
		_enemy = GetComponent<EnemyAttack>();
		transform.localScale = -transform.localScale;
		GetComponent<Rigidbody2D>().velocity *= -1f;

		switch (state)
		{
			case FireballState.ENEMY:
				ChangeState(FireballState.PLAYER);
				break;
			case FireballState.PLAYER:
				ChangeState(FireballState.ENEMY);
				break;
		}
	}
}

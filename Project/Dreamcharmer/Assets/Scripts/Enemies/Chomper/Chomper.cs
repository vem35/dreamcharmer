﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 *  Name:           Chomper.cs
 *  Author:         Josh Megahan
 *  Version:        0.0.5 (extended charge range, removed pre-wait for attacks, reacts to damage)
 *  Last Modified:  28 May 2017
 */
 

// A lot of this is going to be re-implemented
[RequireComponent(typeof(ChomperMovement))]
public class Chomper : Enemy {

    public Chomper() {
        maxHealth = 30;
        health = maxHealth;
    }
    #region variables
    private enum ChomperState { PRECHARGE, CHARGE, FIRE, PATROL, DIE } // Action states
    private enum bound { L, R } // for GetBoundDist

    #region serialized
    [Header("Start Controls")]
    [SerializeField] private bool _isStationary;
    [SerializeField] private bool startRight;
    [Header("Charge")]
    [SerializeField] private GameObject _chargeHitBox;
    [Header("Pathing")]
    [SerializeField] private Transform rightBound;
    [SerializeField] private Transform leftBound;
    [Header("Fireball")]
    [SerializeField] private GameObject fireball;
    [SerializeField] private float fireballSpeed;
    [SerializeField] private int fireballDamage;
    [SerializeField] private Transform pivot;
    #endregion serialized

    #region public
    //is stationary will set chomper to not move, acting as a turret
    public bool isStationary {
        get {
            return _isStationary;
        }
    }
    #endregion public

    #region private
    private ChomperState _currentState;
    private ChomperMovement _move;
    private float _attackTimer;
    private float _setAttackTimer;
    private float _chargeTimer;
    private float _setChargeTimer;
    private int _chargeRange;
    private GameObject _player;
    private VisionTrigger _vision;
    private bool _canAttack;
    private Animator _anim;
    #endregion private

    #endregion variables

    #region methods

    #region stateMethods
    /// <summary> Varibales set when any of the attack states end </summary>
    private void ResetState() {
        if (!isStationary) {
            _move.SetMove();
        } else {
            _move.SetIdle();
        }
        _currentState = ChomperState.PATROL;
    }

    /// <summary> Set to fireball anim - animation event generates fireball. </summary>
    private void Fire() {
        _anim.SetTrigger("Fireball");
        _canAttack = false;
        ResetState();
        StartCoroutine(AttackTimer());
    }

    private void GenerateFireball() {
        GameObject f = Instantiate(fireball, pivot.position, transform.rotation) as GameObject;
        f.transform.localScale = new Vector3(-Mathf.Sign(transform.localScale.x) * f.transform.localScale.x, f.transform.localScale.y, f.transform.localScale.z);
        f.GetComponent<Rigidbody2D>().velocity = new Vector2((_move.leftFace ? -fireballSpeed : fireballSpeed), 0f);
        f.GetComponent<EnemyAttack>().SetDamage(fireballDamage);
        Destroy(f, 3);
    }

    // Activate tell before charge attack
    private void PreCharge() {
        _anim.SetBool("PreCharge", false);
        _currentState = ChomperState.CHARGE;
        _move.SetCharge();
    }

    // Launch charge attack for duration of charge attack timer
    private void Charge() {
        if (_chargeTimer > 0f) {
            _chargeTimer -= Time.deltaTime;
        } else {
            
            _canAttack = false;
            ResetState();
            StartCoroutine(AttackTimer());
        }
    }

    // while walking, check for player in range
    private void Patrol() {
        if(_vision.seePlayer) {
            _move.SetIdle();
            DecideAttack();
        }
    }

    private void SetDie() {
        _move.SetIdle();
        _currentState = ChomperState.DIE;
    }
    #endregion stateMethods

    /// <summary> Retrieve distance between bounding nodes </summary>
    private float GetBoundDist(bound b) {
        if(b == bound.L) { return (leftBound.position.x - transform.position.x); }
        else { return (rightBound.position.x - transform.position.x); }
    }

    /// <summary> Attack Timer </summary>
    private IEnumerator AttackTimer() {
        while (_attackTimer > 0f) {
            _canAttack = false;
            _attackTimer -= Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
        _attackTimer = _setAttackTimer;
        _canAttack = true;
    }

    private void DecideAttack() {
        float boundDist = Mathf.Abs(_move.leftFace ? GetBoundDist(bound.L) : GetBoundDist(bound.R));
        float playerDist = Mathf.Abs(_player.transform.position.x - transform.position.x);

        // and player inside bounding and in charge range then charge, otherwise use fireball
        if (!isStationary && (playerDist < _chargeRange && boundDist > playerDist)) {
            _chargeTimer = _setChargeTimer;
            //set charge hitbox enabled in animator
            _anim.SetBool("PreCharge", true);
            _currentState = ChomperState.PRECHARGE;
        } else {
            _currentState = ChomperState.FIRE;
        }
    }

    private void ReactToDamage() {
        if (!isStationary) {
            if (_player.transform.position.x - transform.position.x > 0f) {
                if (_move.leftFace) {
                    _move.Flip();
                }
            } else if (_player.transform.position.x - transform.position.x < 0f) {
                if (!_move.leftFace) {
                    _move.Flip();
                }
            }
        }
        ResetState();
    }

    #region Monodevelop

    private void Awake() {
        _setAttackTimer = 2f;
        _attackTimer = _setAttackTimer;
        _setChargeTimer = 0.5f;
        _chargeTimer = 0f;
        _chargeRange = 6;
        _move = GetComponent<ChomperMovement>();
        _player = GameObject.Find("Player");
        _vision = this.GetComponentInChildren<VisionTrigger>();
        _currentState = ChomperState.PATROL;

        if (isStationary) {
            _move.SetIdle();
        } else {
            _move.SetMove();
        }

        // Turn on to make Chomper face right when game starts
        if (startRight) {
            _move.Flip();
        }

        _canAttack = true;
        _anim = GetComponent<Animator>();
        _chargeHitBox.SetActive(false);
    }

    private void Update() {
        // Determine if Chomper exceeds boundary distance unless marked as stationary
        if (!isStationary) {
            if (GetBoundDist(bound.L) >= 0) {
                if (_move.leftFace) {
                    ResetState();
                    _move.Flip();
                }
            } else if (GetBoundDist(bound.R) <= 0) {
                if (!_move.leftFace) {
                    ResetState();
                    _move.Flip();
                }
            }
        }

        switch (_currentState) {
            case ChomperState.FIRE:
                if (_canAttack) Fire();
                break;
            case ChomperState.CHARGE:
                if (_canAttack) Charge();
                break;
            case ChomperState.PATROL:
                Patrol();
                break;
            case ChomperState.DIE:
                break;
            default:
                break;
        }
    } 

    #endregion Monodevelope

    #endregion methods
}
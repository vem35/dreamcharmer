﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 *  Name:           ChomperMovement.cs
 *  Author:         Josh Megahan
 *  Version:        0.0.5
 *  Last Modified:  1 July 2017
 */

public class ChomperMovement : Movement {
    public enum MoveState {
        IDLE,
        MOVE,
        CHARGE
    }
    #region variables

    #region public
    
    [Header("Movement")]
    [SerializeField] float moveSpeed;
    [SerializeField] float runSpeed;
    #endregion public

    #region private
    private MoveState _currentState;
    private Rigidbody2D _rb;
    private Animator _anim;
    #endregion private

    #endregion varaibles


    #region methods
    

    private void Move() {
        _rb.velocity = new Vector2(moveSpeed * (leftFace ? -1 : 1), _rb.velocity.y);
    }

    private void Charge() {
        _rb.velocity = new Vector2(runSpeed * (leftFace ? -1 : 1), _rb.velocity.y);
    }

    private void Idle() {
        _rb.velocity = new Vector2(0, _rb.velocity.y);
    }

    #region setState
    public void SetMove() {
        _currentState = MoveState.MOVE;
    }
    public void SetCharge() {
        _currentState = MoveState.CHARGE;
    }
    public void SetIdle() {
        _currentState = MoveState.IDLE;
    }
    #endregion setState

    #region Monodevelop
    private void Awake() {
        _rb = GetComponent<Rigidbody2D>();
        _anim = GetComponentInChildren<Animator>();
    }

    private void Update() {
        _anim.SetBool("Move", false);
        _anim.SetBool("Charge", false);

        switch (_currentState) {
            case MoveState.CHARGE:
                _anim.SetBool("Charge", true);
                Charge();
                break;
            case MoveState.IDLE:
                Idle();
                break;
            case MoveState.MOVE:
                _anim.SetBool("Move", true);
                Move();
                break;
            default:
                break;
        }
    } 
    #endregion Monodevelop

    #endregion methods
}

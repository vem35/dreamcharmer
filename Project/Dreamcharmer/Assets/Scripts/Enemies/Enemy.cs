﻿using System.Collections;
using UnityEngine;

/*
 *		Name:			Enemy.cs
 *		Author:			Vincent Mills
 *		Last Edited:    Josh Megahan
 *		Version:		0.0.4
 *		Last Modified:	3 June 2017
 */

/// <summary> Base Enemy class </summary>
public class Enemy : MonoBehaviour {

    #region Variables
    
    protected Animator anim;
    protected Rigidbody2D rb;
    [SerializeField] protected int maxHealth;
    [SerializeField] protected int health;
    [SerializeField] protected GameObject player;

    public float healthPercent {
        get {
            return health / (float)maxHealth;
        }
    }
    #endregion Variables

    #region Monodevelop
    private void Awake() {
        anim = GetComponentInChildren<Animator>();
        rb = GetComponent<Rigidbody2D>();
    } 

    void OnEnable() {
        // Check if rigidbody compopent exists yet. Assign if null.
        if (rb == null) {
            rb = GetComponent<Rigidbody2D>();
        }
        rb.simulated = true;
        health = maxHealth;
    }
    
    // Enemy takes damage if contact is made
    void OnTriggerEnter2D(Collider2D collision) {
        if (collision.GetComponent<PlayerAttack>() != null) {
            Damage(collision.GetComponent<PlayerAttack>().power);
        }
    }
    #endregion Monodevelop

    #region Methods
    /// <summary> Reduce enemy hit points. If hit points below zero, enemy dies, then deactivates </summary>
    /// <param name="number">The amount of hit points to harm the enemy with.</param>
    public void Damage(int number) {
        health -= number;
        if (health <= 0) {
            anim.SetBool("Dead", true);
            rb.simulated = false;
        } else {
            anim.SetTrigger("Damaged");
        }
    }

    /// <summary> Set gameobject to no longer be active in scene</summary>
    private void Deactivate() {
        gameObject.SetActive(false);
    }
    #endregion Methods
}

/// <summary> Class for Sprite orientation when moving </summary>
public class Movement : MonoBehaviour {
    private bool _leftFace = true;

    public bool leftFace {
        get {
            return _leftFace;
        }
    }

    public void Flip() {
        _leftFace = !_leftFace;
        transform.localScale = new Vector3(-transform.localScale.x, transform.localScale.y, transform.localScale.z);
    }
}

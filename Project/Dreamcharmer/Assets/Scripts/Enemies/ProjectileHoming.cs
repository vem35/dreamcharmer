﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileHoming : MonoBehaviour {
    private GameObject _player;
    private float _lifetime;
    private Rigidbody2D _rb;
    public float speed;

    private void Awake() {
        _rb = GetComponent<Rigidbody2D>();
        _player = GameObject.Find("Player");
        _lifetime = 0f;
    }


    private void Update() {
        _lifetime += Time.deltaTime / 2f;
        Vector2 targetDir = ((_player.transform.position + new Vector3(0f,1f,0f)) - transform.position).normalized;
        Vector2 currentDir = _rb.velocity.normalized;

        _rb.velocity = Vector3.Lerp( currentDir.normalized, targetDir.normalized, Time.deltaTime / _lifetime) * speed;

        Vector2 v = _rb.velocity;
        float angle = Mathf.Atan2(v.y, v.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
        transform.localScale = new Vector3(Mathf.Sign(transform.localScale.x) * transform.localScale.x, transform.localScale.y,transform.localScale.z);

    }
}

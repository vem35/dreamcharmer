﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 *		Name:			Nightmare.cs
 *		Author:		    Josh Megahan
 *		Version:		0.0.2
 *		Last Modified:	5 June 2017
 */

/// <summary> Boss01 - Night Mare </summary>
public class Nightmare : Enemy {

    public Nightmare() {
        health = 200;
        maxHealth = 200;
    }
    // Action States
    public enum State {
        IDLE,
        FIREBALL,
        FIREWHEEL,
        PRECHARGE,
        CHARGE,
        STOMP,
        VULNERABLE,
        DIE
    }

    #region variables
    #region serialized
    [Header("Fireball")]
    [SerializeField] private GameObject fireball;
    [SerializeField] private int fireballDamage;
    [SerializeField] private float fireballSpeed;
    [SerializeField] private Transform mouth;
    [Header("Firewheel")]
    [SerializeField] private int numFireballs;
    [Header("Dash/Charge")]
    [SerializeField] private GameObject threatRangeGO;
    [SerializeField] private float dashSpeed;
    [SerializeField] private float setRechargeTimer;
    [SerializeField] private float dashRechargeTimer;
    [SerializeField] private float boundaryDist;
    [SerializeField] GameObject chargeGO;
    [Header("Vulnerability")]
    [SerializeField] private float setVulnerbaleTimer;
    [SerializeField] private float vulnerableTimer;
    [SerializeField] private GameObject vulnerableGO;
    [Header("Pivot")]
    [SerializeField] private Transform pivot;
	[Header("Stomp")]
	[SerializeField] private Lava lava;

    [SerializeField] private GameObject thanks;
    #endregion serialized

    #region private
    private State _currentState;
    private float _idleTimer;
    private float _setIdleTimer;
    private bool _leftFace;
    private Rigidbody2D _rb;
    private LayerMask _boundary;
    private Transform _mouth;
    private Animator _anim;
    #endregion private
    #endregion variables

    #region methods
    #region state methods
    private void Idle() {
        //Nope
        //Left this here in case Boss might need idle time to space out actions.
    }

    /// <summary> Original intention was to create a state for before
    /// the fight when the boss was talking to the player
    /// </summary>
    private void BeginFight() {
        _anim.SetTrigger("Fireball");
        StartCoroutine(RechargeTimer());
    }

    #region fire
    private void GenerateFireball() {
        GameObject f = Instantiate(fireball, mouth.position, transform.rotation) as GameObject;
        f.transform.localScale = new Vector3(-Mathf.Sign(transform.localScale.x) * f.transform.localScale.x, f.transform.localScale.y, f.transform.localScale.z);
        f.GetComponent<Rigidbody2D>().velocity = new Vector2((_leftFace ? -fireballSpeed : fireballSpeed), 0f);
        f.GetComponent<EnemyAttack>().SetDamage(fireballDamage);
        f.AddComponent<ProjectileHoming>();
        f.GetComponent<ProjectileHoming>().speed = fireballSpeed;
        Destroy(f, 5);
    }

    private void GenerateFirewheel() {
        for (int i = 0; i < numFireballs; i++) {
            Vector2 direction = Vector2.right;

            float angle = ((360 / numFireballs) * i); //degrees
            float sin = Mathf.Sin(angle * Mathf.Deg2Rad);
            float cos = Mathf.Cos(angle * Mathf.Deg2Rad);

            Vector2 forward = new Vector2(
                direction.x * cos - direction.y * sin,
                direction.x * sin + direction.y * cos);

            Quaternion tmpRot = Quaternion.Euler(0f,0f,angle);

            GameObject f = Instantiate(fireball, pivot.position, tmpRot) as GameObject;
            f.GetComponent<Rigidbody2D>().velocity = forward * fireballSpeed;
            
            f.GetComponent<EnemyAttack>().SetDamage(fireballDamage);
            Destroy(f, 5);
        }
    }

    private void Fireball() {
        //Just play the Animation
        //Attack launched in AnimationEvent
    }

    private void Firewheel() {
        //Just play the Animation
        //Attack launched in AnimationEvent
    }
    #endregion fire

    #region charge
    /// <summary> Charge attack cooldown timer </summary>
    private IEnumerator RechargeTimer() {
        dashRechargeTimer = setRechargeTimer;
        while (dashRechargeTimer > 0f) {
            dashRechargeTimer -= Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
        if (dashRechargeTimer <= 0f) {
            dashRechargeTimer = 0f;
        }
    }

    /// <summary> Detrmines if player is in threat range and if cooldown is 0 </summary>
    private bool CanCharge() {
        if (threatRangeGO.GetComponent<ThreatRange>().playerInside && dashRechargeTimer <= 0f) {
            return true;
        }
        return false;
    }

    private void Charge() {
        RaycastHit2D contact;
        Vector2 dir = _leftFace ? Vector2.left : Vector2.right;        
        contact = Physics2D.Raycast(pivot.position, dir, boundaryDist, _boundary.value);
        if (contact.collider != null) {
            _rb.velocity = Vector2.zero;
            Flip();
            StartCoroutine(RechargeTimer());
            chargeGO.SetActive(false);
            ChangeState();
        } else {
            _rb.velocity = new Vector2(dashSpeed * (_leftFace ? -1f : 1f), 0f);
        }
    }
    #endregion charge

    private void Stomp() {
        //Animation event
    }

    public void RaiseLava() {
        lava.Raise();
        lava.Invoke("Lower", 3f);
    }

    #region vulnerability
    /// <summary> Vulnerability timer </summary>
    private IEnumerator VulnerableTimer() {
        vulnerableTimer = setVulnerbaleTimer;
        while (vulnerableTimer > 0f) {
            vulnerableTimer -= Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
        if (vulnerableTimer <= 0f) {
            vulnerableTimer = 0f;
        }
    }

    /// <summary> Vulnerable state - checks if timer reaches zero to change state </summary>
    private void Vulnerable() {
        if (vulnerableTimer == 0f) {
            vulnerableGO.SetActive(false);
            ChangeState();
        }
    }
    #endregion vulnerability

    private void Die() {
        _anim.enabled = false;
        Time.timeScale = 0f;
        thanks.SetActive(true);
    }

    /// <summary> State transition table </summary>
    private void ChangeState() {
        switch (_currentState) {
            case State.IDLE:
                break;
            case State.FIREBALL:
                if (CanCharge()) {
                    _anim.SetBool("Charge", true);
                    chargeGO.SetActive(true);
                    _currentState = State.PRECHARGE;
                } else {
                    _anim.SetTrigger("Firewheel");
                    _currentState = State.FIREWHEEL;
                }
                break;
            case State.FIREWHEEL:
                if (CanCharge()) {
                    _anim.SetBool("Charge", true);
                    _currentState = State.PRECHARGE;}
                else {
                    _anim.SetTrigger("Stomp");
                    _currentState = State.STOMP;
                }
                break;
            case State.PRECHARGE:
                _currentState = State.CHARGE;
                break;
            case State.CHARGE:
                _anim.SetBool("Charge", false);
                _anim.SetBool("Vulnerable", true);
                vulnerableGO.SetActive(true);
                StartCoroutine(VulnerableTimer());
                _currentState = State.VULNERABLE;
                break;
            case State.STOMP:
                _anim.SetBool("Vulnerable", true);
                vulnerableGO.SetActive(true);
                StartCoroutine(VulnerableTimer());
                _currentState = State.VULNERABLE;
                break;
            case State.VULNERABLE:
                _anim.SetBool("Vulnerable", false);
                _anim.SetTrigger("Fireball");
                _currentState = State.FIREBALL;
                break;
            default:
                break;
        }
    }
    #endregion state methods

    public void Flip() {
        _leftFace = !_leftFace;
        transform.localScale = new Vector3(-transform.localScale.x, transform.localScale.y, transform.localScale.z);
    }

    #region Monodevelop
    private void Awake() {
        
        _anim = GetComponent<Animator>();
        _currentState = State.FIREBALL;
        _rb = GetComponent<Rigidbody2D>();
        _boundary = (1 << LayerMask.NameToLayer("Ground"));
        _leftFace = true;

        threatRangeGO = GameObject.Find("ThreatRange");
        numFireballs = 18;
        setRechargeTimer = 15f;
        dashRechargeTimer = setRechargeTimer;
        setVulnerbaleTimer = 5f;
        vulnerableTimer = 0f;
        boundaryDist = 2.5f;
        dashSpeed = 18f;

        BeginFight();
    }


    private void Update() {
        switch(_currentState) {
            case State.IDLE:
                Idle();
                break;
            case State.FIREBALL:
                Fireball();
                break;
            case State.FIREWHEEL:
                Firewheel();
                break;
            case State.CHARGE:
                Charge();
                break;
            case State.PRECHARGE:
                break;
            case State.STOMP:
                Stomp();
                break;
            case State.VULNERABLE:
                Vulnerable();
                break;
            case State.DIE:
                Die();
                break;
            default:
                break;
        }
    }
    #endregion Monodevelop
    #endregion methods
}

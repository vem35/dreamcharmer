﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThreatRange : MonoBehaviour {
    public bool playerInside = false;

    private void OnTriggerEnter2D(Collider2D col) {
        if (col.gameObject.CompareTag("Player")) {
            playerInside = true;
        }
    }

    private void OnTriggerExit2D(Collider2D col) {
        if (col.gameObject.CompareTag("Player")) {
            playerInside = false;
        }
    }
}

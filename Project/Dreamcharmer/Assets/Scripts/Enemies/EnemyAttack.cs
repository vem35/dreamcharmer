﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 *		Name:			EnemyAttack.cs
 *		Author:			Josh Megahan
 *		Version:		0.0.1 (Created)
 *		Last Modified:	29 March 2017
 */

public class EnemyAttack : MonoBehaviour {

    [SerializeField] int _damage;

    public void SetDamage(int damage) {
        _damage = damage;
    }

    public int damage {
        get {
            return _damage;
        }
    }
}

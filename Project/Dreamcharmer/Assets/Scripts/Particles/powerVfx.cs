﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class powerVfx : MonoBehaviour {
	
	//prefab that contains particle Effects for when Lowell's powers hit an enemy
	public GameObject lowellPowerEffect;


	//Function that instantiates and destroys the lowellPowerEffect prefab when Lowell's powers hit an enemy
	void OnTriggerEnter2D(Collider2D other) {
		GameObject a = Instantiate (lowellPowerEffect, transform.position, transform.rotation);
	    Destroy(a, 2);

		//lowellPowerEffect = (GameObject)Instantiate (lowellPowerEffect, transform.position, transform.rotation);

	}
}

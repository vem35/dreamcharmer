﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleExplosion : MonoBehaviour {

	private void OnDestroy()
	{
		GetComponent<ParticleSystem>().Emit(100);
	}
	private void OnDisable()
	{
		GetComponent<ParticleSystem>().Emit(100);
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogueContinue : MonoBehaviour {

	public void ContinuePressed() {
        Time.timeScale = 1f;
        this.gameObject.SetActive(false);
    }
}

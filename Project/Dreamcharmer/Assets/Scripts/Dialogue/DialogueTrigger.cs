﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogueTrigger : MonoBehaviour {

    [SerializeField] GameObject dialogue;
    [SerializeField] GameObject startPause;

    public void OnTriggerEnter2D(Collider2D collision) {
        if (collision.tag == "Player") {
            dialogue.SetActive(true);
            this.gameObject.GetComponent<BoxCollider2D>().enabled = false;
            startPause.GetComponent<StartPause>().SetCanUnpauseFalse();
            Time.timeScale = 0f;
        }
    }
}

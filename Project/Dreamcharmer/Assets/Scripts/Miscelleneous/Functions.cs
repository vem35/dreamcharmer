﻿
/*
 *  Name:           Functions.cs
 *  Author:         Vincent Mills
 *  Version:        0.0.1 (Creation)
 *  Last Modified:  5 May 2017
 */


 /// <summary>
 /// Functions static class.
 ///	For miscelleneous functions that don't necessarily need to be in a class.
 /// </summary>
public static class Functions {

	public static int BoolToInt(bool b)
	{
		return b ? 1 : 0;
	}
}

﻿using UnityEngine;

/*
 *  Name:           DontDestroyOnLoad.cs
 *  Author:         Vincent Mills
 *  Version:        0.0.1 (Creation)
 *  Last Modified:  25 April 2017
 */

public class DontDestroyOnLoad : MonoBehaviour {

	public void Awake()
	{
		DontDestroyOnLoad(gameObject);
	}
}

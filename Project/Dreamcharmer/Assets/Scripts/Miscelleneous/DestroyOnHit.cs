﻿using UnityEngine;

public class DestroyOnHit : MonoBehaviour {

	private void OnTriggerEnter2D(Collider2D c)
	{
		Debug.Log(c);
		gameObject.SetActive(false);
	}

	private void OnCollisionEnter2D(Collision2D c)
	{
		gameObject.SetActive(false);
	}
}

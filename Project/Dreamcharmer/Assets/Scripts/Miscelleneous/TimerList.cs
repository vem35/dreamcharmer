﻿using UnityEngine;
using System.Collections.Generic;

/*
 *  Name:           TimerList.cs
 *  Author:         Vincent Mills
 *  Version:        1.0.0 (Release)
 *  Last Modified:  28 March 2017
 */

/// <summary>
/// The Timer List Class. Keeps track of every timer and sends messages when each are done.
/// <para>Messages:</para>
///		<para> - OnTimerBegin(Timer t): method used for when a timer is started. Determine the timer based on its name.</para>
///		<para> - OnTimerEnd(Timer t): method used for when a timer ends. Determine the timer based on its name.</para>
/// </summary>
public class TimerList : MonoBehaviour
{
	#region Variables

	[SerializeField]List<Timer> timers;

	/// <summary>
	/// Gets the desired timer information via index
	/// </summary>
	/// <param name="i">The timer index</param>
	/// <returns><c>null</c> if the index is out of range. The timer's Timer class otherwise.</returns>
	public Timer this[int i]
	{
		get { return GetTimer(i); }
	}

	/// <summary>
	/// Gets the desired timer information via name.
	/// </summary>
	/// <param name="name">The timer's name</param>
	/// <returns><c>Null</c> if the timer doesn't exist. The timer's Timer class otherwise.</returns>
	public Timer this[string name]
	{
		get { return GetTimer(name); }
	}

	#endregion

	#region Methods

	#region IndexFunctions
	/// <summary>
	/// Gets the desired timer information via index
	/// </summary>
	/// <param name="index">The timer index</param>
	/// <returns><c>null</c> if the index is out of range. The timer's Timer class otherwise.</returns>
	public Timer GetTimer(int index)
	{
		if(index <= 0 || index >= timers.Count) return null;
		return timers[index];
	}

	/// <summary>
	/// Gets the desired timer information via name.
	/// </summary>
	/// <param name="name">The timer's name</param>
	/// <returns><c>Null</c> if the timer doesn't exist. The timer's Timer class otherwise.</returns>
	public Timer GetTimer(string name)
	{
		for(var i = 0; i < timers.Count; ++i)
		{
			if(timers[i].name == name) return timers[i];
		}
		return null;
	}

	#endregion

	void ResetTimer(Timer t)
	{
		SendMessage("OnTimerBegin", t, SendMessageOptions.DontRequireReceiver);
		t.ResetTimer();
	}
	
	#region Monodevelop

	void Awake( )
	{
		for(var i = 0; i < timers.Count; ++i)
		{
			var timer = timers[i];

			ResetTimer(timer);
		}
	}

	void OnEnable( )
	{
		for(var i = 0; i < timers.Count; ++i)
		{
			var timer = timers[i];
			if(timer.state == Timer.TimerState.STOP)
			{
				ResetTimer(timer);
				timer.state = Timer.TimerState.PLAY;
			}
		}
	}

	void LateUpdate( )
	{
		for(var i = 0; i < timers.Count; ++i)
		{
			var timer = timers[i];

			if(timer.state == Timer.TimerState.PLAY)
			{
				if(timer.Update(Time.deltaTime))
				{
					ResetTimer(timer);
					SendMessage("OnTimerEnd", timer, SendMessageOptions.DontRequireReceiver);
					if(!timer.loop) timer.state = Timer.TimerState.STOP;
				}
			}
		}
	}

	void OnValidate( )
	{
		for(var i = 0; i < timers.Count; ++i)
		{
			var timer = timers[i];
			timer.duration = Mathf.Max(timer.duration, 0.001f);
		}
	}

	#endregion

	#endregion Methods
}

/// <summary>
/// Timer class. Keeps track and enables updating the timers.
/// </summary>
[System.Serializable]
public class Timer
{
	#region TimerState
	/// <summary> The state of the timer, whether playing, paused, or stopped. </summary>
	public enum TimerState
	{
		PLAY,
		PAUSE,
		STOP
	}

	#endregion

	#region Variables

	/// <summary> The total maximum time until the timer runs out. </summary>
	public float duration;

	[SerializeField] string _name;

	/// <summary> The unique name/ID of the timer. Get only. </summary>
	public string name { get { return _name; } }

	public TimerState state;

	/// <summary> Whether or not the timer should loop when it finishes. </summary>
	public bool loop;

	float _time;

	/// <summary> The amount of time remaining until the timer runs out. Returns a number less than zero if it ended already. <para>Get only.</para> </summary>
	public float remain { get { return _time; } }

	#endregion

	#region Methods

	/// <summary> Updates the timer by the given amount of time. </summary>
	/// <param name="t">The amount of time to update.</param>
	/// <returns><c>true</c> if the timer ran out. <c>false</c> otherwise.</returns>
	public bool Update(float t)
	{
		_time -= t;
		return _time <= 0f;
	}

	/// <summary> Resets the timer to the beginning </summary>
	public void ResetTimer( )
	{
		_time = duration;
	}

	#endregion
}
﻿using UnityEngine;

public class TransformChange : MonoBehaviour {

	[SerializeField] bool scaledTime;

	[SerializeField] Vector3 positionChange;
	[SerializeField] Vector3 rotationChange;
	[SerializeField] Vector3 scaleChange;


	
	// Update is called once per frame
	void Update () {
		float _dt = scaledTime ? Time.deltaTime : Time.unscaledDeltaTime;

		if(positionChange != Vector3.zero)
			transform.Translate(positionChange * 60f * _dt);
		transform.Rotate(rotationChange * 60f * _dt);
		transform.localScale = transform.localScale + scaleChange * 60f * _dt;
	}
}

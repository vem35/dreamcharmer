﻿using UnityEngine;

public class DeactivateOutside : MonoBehaviour {

	private void Update()
	{
        Vector3 pos = Camera.main.WorldToScreenPoint(transform.position);

        if (pos.x > Screen.width || pos.y > Screen.height)
            gameObject.SetActive(false);
		else if (pos.x < 0 || pos.y < 0)
            gameObject.SetActive(false);
	}
}

﻿using UnityEngine;
using UnityEngine.UI;

/*
 *  Name:           UI_Health.cs
 *  Author:         Vincent Mills
 *  Version:        0.0.1 (Creation)
 *  Last Modified:  25 April 2017
 */

[RequireComponent(typeof(Image))]
public class UI_Health : MonoBehaviour {

	Image image;
	Player player;

	private void Awake()
	{
		image = GetComponent<Image>();
	}

	// Use this for initialization
	void Start () {
		player = Player.main ? Player.main : null;
	}
	
	// Update is called once per frame
	void OnGUI () {
		if(player)
		{
			image.fillAmount = player ? (float)player.stats.health / (float)player.stats.healthMax : 1f;
            //image.color = ImageColor();
            image.color = new Color(0.5f, 1f, 0.5f);
		}
	}

	private Color ImageColor()
	{
		float r, g, b;

		if(image.fillAmount > 0.5f)
		{
			r = -2f * image.fillAmount + 2f;
			g = 1f;
			b = 0f;
		}
		else
		{
			r = 1f;
			g = image.fillAmount * 2f;
			b = 0f;
		}

		return new Color(r, g, b);
	}
}

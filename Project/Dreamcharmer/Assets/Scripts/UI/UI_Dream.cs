﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UI_Dream : MonoBehaviour {

	Animator _anim;
	[SerializeField] DreamColor _color;

	private void Awake()
	{
		_anim = GetComponent<Animator>();
		if (DreamAnimations.main == null)
			DreamAnimations.main = new DreamAnimations();
		DreamAnimations.main[_color] = _anim;
	}

	private void OnDestroy()
	{
		DreamAnimations.main[_color] = null;
	}

	private void OnDisable()
	{
		DreamAnimations.main[_color] = null;
	}

	private void OnEnable()
	{
		DreamAnimations.main[_color] = _anim;
	}
}

public class DreamAnimations
{
	public static DreamAnimations main;

	public Dictionary<DreamColor, Animator> anim;

	public Animator this[DreamColor d]
	{
		get
		{
			return anim[d];
		}
		set
		{
			if (anim.ContainsKey(d))
				anim[d] = value;
			else
				anim.Add(d, value);
		}
	}

	public DreamAnimations()
	{
		anim = new Dictionary<DreamColor, Animator>();
	}

	public void Glow(DreamColor d)
	{
		if (this[d])
			this[d].SetTrigger("Glow");
	}
}
﻿using UnityEngine;
using UnityEngine.UI;

public class UI_Boss_Health : MonoBehaviour {

	Image image;
	[SerializeField] Enemy enemy;

	private void Awake()
	{
		image = GetComponent<Image>();
	}

	// Update is called once per frame
	void OnGUI()
	{
		image.fillAmount = enemy ? (float)enemy.healthPercent : 0.5f;
		image.color = ImageColor();
	}

	private Color ImageColor()
	{
		float r, g, b;

		if (image.fillAmount > 0.5f)
		{
			r = -2f * image.fillAmount + 2f;
			g = 1f;
			b = 0f;
		}
		else
		{
			r = 1f;
			g = image.fillAmount * 2f;
			b = 0f;
		}

		return new Color(r, g, b);
	}
}

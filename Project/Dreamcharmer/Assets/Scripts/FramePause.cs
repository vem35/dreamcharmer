﻿using System.Collections;
using UnityEngine;

public class FramePause {
    public IEnumerator PauseOnHit() {
        float i = 0;
        while (i<1) {
            Time.timeScale = 0f;
            i++;
            yield return new WaitForSecondsRealtime(0.5f);
        }
        Time.timeScale = 1f;
    }
}

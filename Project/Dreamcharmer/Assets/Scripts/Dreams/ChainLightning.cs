﻿using UnityEngine;
using System.Collections.Generic;

[RequireComponent(typeof(Fireball))]
public class ChainLightning : MonoBehaviour {

	[SerializeField] string tagToDamage;
	[SerializeField] float lightningRadius;
	[SerializeField] uint maxHits;
	[SerializeField] GameObject explosion;

	private void OnTriggerEnter2D(Collider2D collision)
	{
		Enemy enemy = collision.GetComponent<Enemy>();
		if (enemy == null)
			enemy = collision.GetComponentInParent<Enemy>();

		if (enemy)
		{
			Debug.Log("Hello");
			enemy.Damage(GetComponent<Fireball>().power);
			var l = new List<Enemy>();
			l.Add(enemy);
			Chain(collision.transform.position, maxHits, l);
			gameObject.SetActive(false);
			if (explosion)
			{

				var a = Instantiate(explosion, enemy.transform.position, enemy.transform.rotation) as GameObject;
				Destroy(a, 1.5f);
			}
		}

		gameObject.SetActive(false);
	}

	public void Chain(Vector2 position, uint hits, List<Enemy> included)
	{
		if (hits <= 0)
			return;
		Collider2D[] c = Physics2D.OverlapCircleAll(position, lightningRadius);
		if (c == null) return;
		
		foreach(var i in c)
		{
			Enemy e = i.GetComponent<Enemy>();
			if (e == null)
				e = i.GetComponentInParent<Enemy>();
			if(e != null && !IsIncluded(e, included))
			{
				e.Damage(GetComponent<Fireball>().power);
				included.Add(e);
				if (explosion != null)
				{

					var a = Instantiate(explosion, e.transform.position, e.transform.rotation) as GameObject;
					Destroy(a, 1.5f);
				}
				Chain(i.transform.position, hits - 1, included);
				return;
			}
		}
	}

	private bool IsIncluded(Enemy e, List<Enemy> list)
	{
		foreach(var i in list)
		{
			if (e == i) return true;
		}
		return false;
	}
}

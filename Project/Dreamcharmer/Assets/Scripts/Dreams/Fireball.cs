﻿using UnityEngine;

/*
 *  Name:           Fireball.cs
 *  Author:         Vincent Mills
 *  Version:        0.0.2 (Pellet class)
 *  Last Modified:  5 May 2017
 */

public class Fireball : PlayerAttack {

	public void Disable()
	{
		gameObject.SetActive(false);
	}

	void OnTimerEnd(Timer t)
	{
		if(t.name == "disable")
		{
			t.ResetTimer();
			Disable();
		}
	}
}

/// <summary>
/// Pellet class.
///		For use with the shotgun power.
/// </summary>
[System.Serializable]
public class Pellet
{
	public GameObject gameObject;
	public float rotation;
	public Rigidbody2D _rb;

	public Pellet(GameObject obj, float angle)
	{
		gameObject = obj;
		rotation = angle;
		if (rotation == 360 || rotation == 0)
		{
			rotation = 0;
		}
		else if(rotation > 360 || rotation < 0)
		{
			while (rotation > 360)
				rotation -= 360;

			while (rotation < 0)
				rotation += 0;
		}
		_rb = gameObject.GetComponent<Rigidbody2D>();
	}

	public void Shoot(float speed)
	{
		_rb.velocity = Quaternion.Euler(0f, 0f, rotation) * Vector2.right * speed;
	}
}
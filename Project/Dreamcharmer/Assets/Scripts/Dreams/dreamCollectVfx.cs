﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class dreamCollectVfx : MonoBehaviour {
	
		//prefab that contains particle Effects for when Lowell collects dreams
		[SerializeField] GameObject dreamCollectEffect;


	//Function that instantiates and destroys the dreamCollect prefab when Lowell collects dreams
	void OnTriggerEnter2D(Collider2D other) {
		if (other.gameObject.tag == "Player") {
            
			GameObject a = Instantiate (dreamCollectEffect, transform.position, transform.rotation);

			Destroy(a, 1);

		
		}
	}



	}
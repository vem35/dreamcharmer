﻿using System.Collections.Generic;
using UnityEngine;

public class DreamSpawner : MonoBehaviour {

	GameObject currentDream;
	TimerList _timers;

	[SerializeField] float cooldownTimer;
	[SerializeField] List<ColorTracker> trackers;

	[SerializeField] bool limited = false;
	[SerializeField] int dreamsMax;
	private int _currentDreamNumber;
	public int currentDreamNumber
	{
		get
		{
			return _currentDreamNumber;
		}
		private set
		{
			if(limited)
				Debug.Log(value);
			_currentDreamNumber = value;
			if (limited && _currentDreamNumber > dreamsMax)
				StopAll();
		}
	}

	//string[] colors = { "red", "blue", "green", "yellow" };

	private void Awake()
	{
		_timers = GetComponent<TimerList>();
	}

	private void StopAll()
	{
		_timers["cooldown"].state = Timer.TimerState.STOP;
		foreach(var a in trackers)
		{
			_timers[a.timerName].state = Timer.TimerState.STOP;
		}
	}

	private void Start()
	{
		_timers["cooldown"].duration = cooldownTimer;
		_timers["cooldown"].ResetTimer();

		foreach (var a in trackers)
		{
			_timers[a.timerName].state = Timer.TimerState.PAUSE;
			_timers[a.timerName].duration = a.timeDuration;
			_timers[a.timerName].ResetTimer();

			if (a.spawn)
				_timers[a.timerName].state = Timer.TimerState.PLAY;
		}
		currentDreamNumber = 0;
	}

	void OnTimerEnd(Timer t)
	{
		if (t.name == "cooldown")
		{
			foreach (var a in trackers)
				if (a.spawn)
					_timers[a.timerName].state = Timer.TimerState.PLAY;
		}
		else
		{
			foreach (var a in trackers)
			{
				if (a.timerName == t.name)
				{
					Debug.Log(a.timerName);
					currentDream = Instantiate(a.dream, transform.position, transform.rotation) as GameObject;
					var c = _timers["cooldown"];
					c.state = Timer.TimerState.PAUSE;
					c.ResetTimer();
					currentDreamNumber++;
				}
				else
				{
					_timers[a.timerName].state = Timer.TimerState.PAUSE;
				}
			}
		}
		t.ResetTimer();
	}

	private void Update()
	{
		if (currentDream && currentDream.activeSelf == false)
		{
			Destroy(currentDream);
			if(!(limited && currentDreamNumber > dreamsMax))
				_timers["cooldown"].state = Timer.TimerState.PLAY;
		}
	}

	public void ResetDreamLimit()
	{
		currentDreamNumber = 0;
		Start();
	}
}

[System.Serializable]
public struct ColorTracker
{
	public string timerName;
	public GameObject dream;
	public float timeDuration;
	public bool spawn;
}

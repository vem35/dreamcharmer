﻿using System.Collections;
using UnityEngine;

public class Float : MonoBehaviour {
    [SerializeField] bool floatUp = false;
    [SerializeField] float speed = 0.05f;
    [SerializeField] float seconds = 3f;
	
	private void Update () {
        StartCoroutine( floatUp ? FloatUp(speed, transform.position) : FloatDown(-speed, transform.position) );
	}

    private IEnumerator FloatUp(float spd, Vector3 tmp) {
        transform.position = new Vector3(tmp.x, tmp.y + spd * Time.deltaTime, tmp.z);
        yield return new WaitForSeconds(seconds);
        floatUp = false;
    }

    private IEnumerator FloatDown(float spd, Vector3 tmp) {
        transform.position = new Vector3(tmp.x, tmp.y + spd * Time.deltaTime, tmp.z);
        yield return new WaitForSeconds(seconds);
        floatUp = true;
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Reflector : MonoBehaviour {

	BoxCollider2D _b;
	// Use this for initialization
	void Start () {
		_b = GetComponent<BoxCollider2D>();
	}
	
	// Update is called once per frame
	void Update () {
		
		var a = Physics2D.OverlapBox((Vector2)transform.position - _b.size/2 + _b.offset, _b.size, 0f, LayerMask.GetMask("EnemyBullet"));
		if (a && a.GetComponent<EnemyFireball>() != null)
		{
			a.GetComponent<EnemyFireball>().Reflect();
			//gameObject.SetActive(false);
		}
	}
}

﻿using UnityEngine;

[RequireComponent(typeof(SpriteRenderer))]
public class Dream : MonoBehaviour {

    [SerializeField] DreamStat _stats;
    public DreamStat stats { get { return _stats; } }
	[SerializeField] bool changeColor = false;

    SpriteRenderer _render;

    private void Awake()
    {
        _render = GetComponent<SpriteRenderer>();
    }
    // Use this for initialization
    void Start () {
		if(changeColor)
	        _render.color = _stats.Color;
	}

	public static DreamColor ColorAfter(DreamColor d)
	{
		switch (d)
		{
			case DreamColor.RED:
				return DreamColor.YELLOW;
			case DreamColor.YELLOW:
				return DreamColor.GREEN;
			case DreamColor.GREEN:
				return DreamColor.BLUE;
			case DreamColor.BLUE:
				return DreamColor.RED;
			default:
				return DreamColor.RED;
		}
	}

	public static DreamColor ColorBefore(DreamColor d)
	{
		switch (d)
		{
			case DreamColor.RED:
				return DreamColor.BLUE;
			case DreamColor.YELLOW:
				return DreamColor.RED;
			case DreamColor.GREEN:
				return DreamColor.YELLOW;
			case DreamColor.BLUE:
				return DreamColor.GREEN;
			default:
				return DreamColor.RED;
		}
	}
}

/// <summary>
/// The possible colors of the dreams
/// </summary>
public enum DreamColor
{
	RED,
	BLUE,
	GREEN,
	YELLOW,
	NONE
}

[System.Serializable]
public struct DreamStat
{
    public DreamColor dreamColor;
    public int amount;

    public Color Color
    {
        get
        {
            switch(dreamColor)
            {
                case DreamColor.RED:
                    return Color.red;
                case DreamColor.BLUE:
                    return Color.blue;
				case DreamColor.YELLOW:
					return Color.yellow;
                case DreamColor.GREEN:
                    return Color.green;
				default:
					return Color.white;
            }
        }
    }
}

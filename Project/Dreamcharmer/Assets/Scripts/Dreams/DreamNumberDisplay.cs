﻿using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class DreamNumberDisplay : MonoBehaviour {

	[SerializeField] bool checkSelected;
	[SerializeField] DreamColor _color;
	DreamColor color;

	Text text;

	private void Awake()
	{
		text = GetComponent<Text>();
	}

	private void Update()
	{
		color = _color;
	}

	private void OnGUI()
	{
		text.text = color == DreamColor.NONE ? "" : Player.main.stats[color].ToString();	
	}
}

﻿using UnityEngine.SceneManagement;
using UnityEngine;

public class Door : Interactable {

	[SerializeField] string sceneChange;

	public override void Interact()
	{
		Player.main.stats.Save();
		SceneManager.LoadScene(sceneChange);
	}
}

﻿using UnityEngine;

/*
 *      Name:           BuildingFallThrough.cs
 *      Author:         Vincent Mills
 *      Version:        0.1.0 (Documented code.)
 *      Last Modified:  27 June 2017
 */

[RequireComponent(typeof(PlatformEffector2D))]
public class BuildingFallThrough : MonoBehaviour {

	enum FallState
	{
		NORMAL,
		FALLTHROUGH
	}

	[SerializeField] FallState _state;

	private PlatformEffector2D _effector;
	int defaultMask;

	#region methods

	#region Monobehavior
	private void Awake()
	{
		_effector = GetComponent<PlatformEffector2D>();

	}

	// Use this for initialization
	void Start()
	{
		defaultMask = _effector.colliderMask;
		_state = FallState.NORMAL;
	}

	// Update is called once per frame
	void Update()
	{
		switch (_state)
		{
			case FallState.FALLTHROUGH:
				BoxCollider2D _b = GetComponent<BoxCollider2D>();
				var a = Physics2D.OverlapBox((Vector2)transform.position + _b.offset, _b.size + new Vector2(0.2f, 0.2f), 0f, LayerMask.GetMask("Player"));
				Debug.Log(a == null);
				if (a == null)
				{
					_effector.colliderMask = defaultMask;
					_state = FallState.NORMAL;
				}
				break;
			case FallState.NORMAL:
				//Debug.Log("ready");
				break;
		}
	}
	#endregion

	public void Fall()
	{
		Exclude("Player");
		_state = FallState.FALLTHROUGH;
	}

	private bool MaskContains(string maskName)
	{
		return _effector.colliderMask == (_effector.colliderMask | (1 << LayerMask.NameToLayer(maskName)));
	}

	private void Include(params string[] maskNames)
	{
		_effector.useColliderMask = true;
		_effector.colliderMask = _effector.colliderMask | (LayerMask.GetMask(maskNames));
	}

	private void Exclude(params string[] maskNames)
	{
		_effector.useColliderMask = true;
		_effector.colliderMask = _effector.colliderMask & ~(LayerMask.GetMask(maskNames));
	}

	#endregion methods
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeatedBlock : EnemyAttack {

	[SerializeField] private string normalTag;
	[SerializeField] private Sprite normalSprite;

	[SerializeField] private string heatedTag;
	[SerializeField] private Sprite heatedSprite;

	public void Heat()
	{
		tag = heatedTag;
		var t = GetComponent<TimerList>();
		if(t != null)
		{
			var timer = t["cool"];
			if(timer != null)
			{
				timer.ResetTimer();
				timer.state = Timer.TimerState.PLAY;
			}
		}

		var s = GetComponent<SpriteRenderer>();
		if(s)
		{
			s.sprite = heatedSprite;
		}
	}

	public void Cool()
	{
		tag = normalTag;
		var s = GetComponent<SpriteRenderer>();
		if (s)
		{
			s.sprite = normalSprite;
		}
	}

	public void OnTimerEnd(Timer t)
	{
		if(t.name == "cool")
		{
			Cool();
			t.ResetTimer();
		}
	}
}

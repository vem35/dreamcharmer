﻿using UnityEngine;

public class LavaBubble : MonoBehaviour {

	Animator _anim;

	private void Awake()
	{
		_anim = GetComponent<Animator>();
	}

	void OnTimerEnd(Timer t)
	{
		if (t.name == "pop")
			_anim.SetTrigger("Pop");
	}
}

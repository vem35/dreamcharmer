﻿using UnityEngine;

public class Lava : EnemyAttack {

	[SerializeField] Animator _anim;

	public void Raise()
	{
		_anim.SetBool("tall", true);
	}
	public void Lower()
	{
		if(_anim)
		{
			_anim.SetBool("tall", false);
		}
	}
}

﻿using UnityEngine.SceneManagement;
using UnityEngine;

public class SceneManagement : MonoBehaviour {

    //[SerializeField] string scene;

    public void SceneChange(string s) {
        SceneManager.LoadScene(s);
    }

    public void QuitGame() {
        Application.Quit();
    }
}

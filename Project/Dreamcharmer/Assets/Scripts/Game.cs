﻿using UnityEngine;
using UnityEngine.SceneManagement;

/*
 *      Name:           Game.cs
 *      Author:         Vincent Mills
 *      Version:        0.1.0 (Documented code.)
 *      Last Modified:  27 June 2017
 */

/// <summary>
///		Game class.
/// </summary>
/// <remarks>
///		Singleton class.
/// </remarks>
public class Game : MonoBehaviour {
    [SerializeField] GameObject pauseMenu;
    [SerializeField] GameObject startPause;

	public static Game main;

	private void Awake()
	{
		if(main == null)
		{
			main = this;
		}
		else
		{
			if(main != this)
			{
				Destroy(gameObject);
			}
		}
	}

	// Use this for initialization
	void Start () {
        pauseMenu.SetActive(false);
    }

	// Update is called once per frame
	void Update()
	{
		if (Controls.GetKeyDown("Pause") && startPause.GetComponent<StartPause>().canUnpause)
		{
            if (Time.timeScale == 1f) {
				Pause();
            } else {
				Resume();
            }
		}
	}

	public void Pause()
	{
		pauseMenu.SetActive(true);
		Time.timeScale = 0f;
	}

	public void Resume() {
        pauseMenu.SetActive(false);
        Time.timeScale = 1f;
    }

	public void ChangeScene(string scene)
	{
		SceneManager.LoadScene(scene);
	}

	public void Quit()
	{
		Application.Quit();
	}
}

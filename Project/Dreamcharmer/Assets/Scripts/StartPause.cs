﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartPause : MonoBehaviour {
    public bool canUnpause = false;

    public void Start() {
        if (!canUnpause) {
            Time.timeScale = 0f;
        } else {
            Time.timeScale = 1f;
        }
    }

    public void SetCanUnpauseTrue() {
        canUnpause = true;
    } 

    public void SetCanUnpauseFalse() {
        canUnpause = false;
    }
}
